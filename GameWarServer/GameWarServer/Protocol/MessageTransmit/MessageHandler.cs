﻿using CarNetwork;
using GameProtocol;
using System;
using System.IO;

public class MessageHandler : Singleton<MessageHandler>
{
    public event Action<CarTcpProtocol> OnEmptyRequestEvent;
    public event Action<CarTcpProtocol> OnErrorRequestEvent;
    public event Action<CarTcpProtocol, RegisterRequest> OnRegisterRequestEvent;
    public event Action<CarTcpProtocol, LoginRequest> OnLoginRequestEvent;
    public event Action<CarTcpProtocol, GetRoleInfoRequest> OnGetRoleInfoRequestEvent;
    public event Action<CarTcpProtocol, EnterGameRequest> OnEnterGameRequestEvent;
    public event Action<CarTcpProtocol, SyncTransformRequest> OnSyncTransformRequestEvent;
    public event Action<CarTcpProtocol, MoveToRequest> OnMoveToRequestEvent;
    public event Action<CarTcpProtocol, MoveTrackRequest> OnMoveTrackRequestEvent;
    public event Action<CarTcpProtocol, MoveTowardsRequest> OnMoveTowardsRequestEvent;
    public event Action<CarTcpProtocol, MoveStopRequest> OnMoveStopRequestEvent;
    public event Action<CarTcpProtocol, SyncPropertyRequest> OnSyncPropertyRequestEvent;
    public event Action<CarTcpProtocol, AttackRequest> OnAttackRequestEvent;
    public event Action<CarTcpProtocol, CastSkillRequest> OnCastSkillRequestEvent;

    public void Start()
    {
        GameNetwork.Instance.RegisterHandler((int)Packet.EmptyRequest, OnHandleEmptyRequest);
        GameNetwork.Instance.RegisterHandler((int)Packet.OnErrorEvent, OnHandleErrorRequest);
        GameNetwork.Instance.RegisterHandler((int)Packet.RegisterRequest, OnHandleRegisterRequest);
        GameNetwork.Instance.RegisterHandler((int)Packet.LoginRequest, OnHandleLoginRequest);
        GameNetwork.Instance.RegisterHandler((int)Packet.GetRoleInfoRequest, OnHandleGetRoleInfoRequest);
        GameNetwork.Instance.RegisterHandler((int)Packet.GetActorInfoRequest, OnHandleGetActorInfoRequest);
        GameNetwork.Instance.RegisterHandler((int)Packet.SyncTransformRequest, OnHandleSyncTransformRequest);
        GameNetwork.Instance.RegisterHandler((int)Packet.MoveToRequest, OnHandleMoveToRequest);
        GameNetwork.Instance.RegisterHandler((int)Packet.MoveTrackRequest, OnHandleMoveTrackReuqest);
        GameNetwork.Instance.RegisterHandler((int)Packet.MoveTowardsRequest, OnHandleMoveTowardsRequest);
        GameNetwork.Instance.RegisterHandler((int)Packet.MoveStopRequest, OnHandleMoveStopRequest);
        GameNetwork.Instance.RegisterHandler((int)Packet.SyncPropertyRequest, OnHandleSyncPropertyRequest);
        GameNetwork.Instance.RegisterHandler((int)Packet.AttackRequest, OnHandleAttackRequest);
        GameNetwork.Instance.RegisterHandler((int)Packet.CastSkillRequest, OnHandleCastSkillRequest);
    }

    public void Destroy()
    {

    }

    public void OnHandleEmptyRequest(CarTcpProtocol protocol, CarBuffer buffer, BinaryReader reader, int request)
    {
        if (OnEmptyRequestEvent != null)
        {
            OnEmptyRequestEvent(protocol);
        }
    }

    public void OnHandleErrorRequest(CarTcpProtocol protocol, CarBuffer buffer, BinaryReader reader, int request)
    {
        if (OnErrorRequestEvent != null)
        {
            OnErrorRequestEvent(protocol);
        }
    }

    public void OnHandleRegisterRequest(CarTcpProtocol protocol, CarBuffer buffer, BinaryReader reader, int request)
    {
        if (OnRegisterRequestEvent != null)
        {
            RegisterRequest registerRequest = CarMessageConverter.Instance.Deserialize<RegisterRequest>(buffer);
            OnRegisterRequestEvent(protocol, registerRequest);
        }
    }

    public void OnHandleLoginRequest(CarTcpProtocol protocol, CarBuffer buffer, BinaryReader reader, int request)
    {
        if (OnLoginRequestEvent != null)
        {
            LoginRequest loginRequest = CarMessageConverter.Instance.Deserialize<LoginRequest>(buffer);
            OnLoginRequestEvent(protocol, loginRequest);
        }
    }

    public void OnHandleGetRoleInfoRequest(CarTcpProtocol protocol, CarBuffer buffer, BinaryReader reader, int request)
    {
        if (OnGetRoleInfoRequestEvent != null)
        {
            GetRoleInfoRequest getRoleInfoRequest = CarMessageConverter.Instance.Deserialize<GetRoleInfoRequest>(buffer);
            OnGetRoleInfoRequestEvent(protocol, getRoleInfoRequest);
        }
    }

    public void OnHandleGetActorInfoRequest(CarTcpProtocol protocol, CarBuffer buffer, BinaryReader reader, int request)
    {
        if (OnEnterGameRequestEvent != null)
        {
            EnterGameRequest enterGameRequest = CarMessageConverter.Instance.Deserialize<EnterGameRequest>(buffer);
            OnEnterGameRequestEvent(protocol, enterGameRequest);
        }
    }

    public void OnHandleSyncTransformRequest(CarTcpProtocol protocol, CarBuffer buffer, BinaryReader reader, int request)
    {
        if (OnSyncTransformRequestEvent != null)
        {
            SyncTransformRequest syncTransformRequest = CarMessageConverter.Instance.Deserialize<SyncTransformRequest>(buffer);
            OnSyncTransformRequestEvent(protocol, syncTransformRequest);
        }
    }

    public void OnHandleMoveToRequest(CarTcpProtocol protocol, CarBuffer buffer, BinaryReader reader, int request)
    {
        if (OnMoveToRequestEvent != null)
        {
            MoveToRequest moveToRequest = CarMessageConverter.Instance.Deserialize<MoveToRequest>(buffer);
            OnMoveToRequestEvent(protocol, moveToRequest);
        }
    }

    public void OnHandleMoveTrackReuqest(CarTcpProtocol protocol, CarBuffer buffer, BinaryReader reader, int request)
    {
        if (OnMoveTrackRequestEvent != null)
        {
            MoveTrackRequest moveTrackRequest = CarMessageConverter.Instance.Deserialize<MoveTrackRequest>(buffer);
            OnMoveTrackRequestEvent(protocol, moveTrackRequest);
        }
    }

    public void OnHandleMoveTowardsRequest(CarTcpProtocol protocol, CarBuffer buffer, BinaryReader reader, int request)
    {
        if (OnMoveTowardsRequestEvent != null)
        {
            MoveTowardsRequest moveTowardsResquest = CarMessageConverter.Instance.Deserialize<MoveTowardsRequest>(buffer);
            OnMoveTowardsRequestEvent(protocol, moveTowardsResquest);
        }
    }

    public void OnHandleMoveStopRequest(CarTcpProtocol protocol, CarBuffer buffer, BinaryReader reader, int request)
    {
        if (OnMoveStopRequestEvent != null)
        {
            MoveStopRequest moveStopRequest = CarMessageConverter.Instance.Deserialize<MoveStopRequest>(buffer);
            OnMoveStopRequestEvent(protocol, moveStopRequest);
        }
    }

    public void OnHandleSyncPropertyRequest(CarTcpProtocol protocol, CarBuffer buffer, BinaryReader reader, int request)
    {
        if (OnSyncPropertyRequestEvent != null)
        {
            SyncPropertyRequest syncPropertyRequest = CarMessageConverter.Instance.Deserialize<SyncPropertyRequest>(buffer);
            OnSyncPropertyRequestEvent(protocol, syncPropertyRequest);
        }
    }

    public void OnHandleAttackRequest(CarTcpProtocol protocol, CarBuffer buffer, BinaryReader reader, int request)
    {
        if (OnAttackRequestEvent != null)
        {
            AttackRequest attackRequest = CarMessageConverter.Instance.Deserialize<AttackRequest>(buffer);
            OnAttackRequestEvent(protocol, attackRequest);
        }
    }

    public void OnHandleCastSkillRequest(CarTcpProtocol protocol, CarBuffer buffer, BinaryReader reader, int request)
    {
        if (OnCastSkillRequestEvent != null)
        {
            CastSkillRequest castSkillRequest = CarMessageConverter.Instance.Deserialize<CastSkillRequest>(buffer);
            OnCastSkillRequestEvent(protocol, castSkillRequest);
        }
    }
}