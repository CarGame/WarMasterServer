﻿using System.Collections.Generic;

public class CarCache
{
    private int mCacheCapacity = 50;
    private LinkedList<CarCacheItem> mCacheItemLinkedList = new LinkedList<CarCacheItem>();

    public void RecycleItem(string stringKey, object objectValue)
    {
        if (objectValue != null && !ContainsItem(objectValue))
        {
            if (mCacheItemLinkedList.Count >= mCacheCapacity)
            {
                mCacheItemLinkedList.RemoveLast();
            }

            CarCacheItem cacheItem = new CarCacheItem();
            cacheItem.stringKey = stringKey;
            cacheItem.objectValue = objectValue;

            mCacheItemLinkedList.AddFirst(cacheItem);
        }
    }

    public void RecycleItem(int intKey, object objectValue)
    {
        if (objectValue != null && !ContainsItem(objectValue))
        {
            if (mCacheItemLinkedList.Count >= mCacheCapacity)
            {
                mCacheItemLinkedList.RemoveLast();
            }

            CarCacheItem cacheItem = new CarCacheItem();
            cacheItem.intKey = intKey;
            cacheItem.objectValue = objectValue;

            mCacheItemLinkedList.AddFirst(cacheItem);
        }
    }

    public object GetItemByInt(int intKey)
    {
        LinkedListNode<CarCacheItem> cacheItemNode = mCacheItemLinkedList.First;

        int length = mCacheItemLinkedList.Count;
        for (int i = 0; i < length; ++i)
        {
            if (cacheItemNode != null && cacheItemNode.Value != null)
            {
                CarCacheItem cacheItem = cacheItemNode.Value;
                if (cacheItem.intKey == intKey)
                {
                    mCacheItemLinkedList.Remove(cacheItemNode);
                    return cacheItem.objectValue;
                }

                cacheItemNode = cacheItemNode.Next;
            }
        }

        return null;
    }

    public object GetItemByString(string stringKey)
    {
        LinkedListNode<CarCacheItem> cacheItemNode = mCacheItemLinkedList.First;

        int length = mCacheItemLinkedList.Count;
        for (int i = 0; i < length; ++i)
        {
            if (cacheItemNode != null && cacheItemNode.Value != null)
            {
                CarCacheItem cacheItem = cacheItemNode.Value;
                if (cacheItem.stringKey == stringKey)
                {
                    mCacheItemLinkedList.Remove(cacheItemNode);
                    return cacheItem.objectValue;
                }

                cacheItemNode = cacheItemNode.Next;
            }
        }

        return null;
    }

    public object GetItem()
    {
        if (mCacheItemLinkedList.Count > 0)
        {
            LinkedListNode<CarCacheItem> cacheItemNode = mCacheItemLinkedList.First;
            if (cacheItemNode != null)
            {
                CarCacheItem cacheItem = cacheItemNode.Value;
                if (cacheItem != null)
                {
                    return cacheItem.objectValue;
                }
            }
        }

        return null;
    }

    public void ClearItem()
    {
        mCacheItemLinkedList.Clear();
    }

    public bool ContainsItem(object objectValue)
    {
        foreach (CarCacheItem cacheItemNode in mCacheItemLinkedList)
        {
            if (cacheItemNode != null)
            {
                if (cacheItemNode == objectValue)
                {
                    return true;
                }
            }
        }

        return false;
    }

    public void SetCapacity(int capacity)
    {
        mCacheCapacity = capacity;
    }
}
