using System.Net;

namespace CarNetwork
{
    public struct CarDatagram
    {
        public CarBuffer buffer;
        public IPEndPoint ip;
    }
}