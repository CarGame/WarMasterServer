using System;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;

namespace CarNetwork
{
    public class CarUdpProtocol
    {
        public event Action<string> OnErrorEvent;

        private int mPort = -1;
        private Socket mSocket;

        private byte[] mTempCache = new byte[8192];

        private EndPoint mEndPoint = new IPEndPoint(IPAddress.Any, 0);
        private IPEndPoint mBroadcastIP = new IPEndPoint(IPAddress.Broadcast, 0);

        private Queue<CarDatagram> mInQueue = new Queue<CarDatagram>();
        private Queue<CarDatagram> mOutQueue = new Queue<CarDatagram>();

        public bool IsActive
        {
            get
            {
                return mPort != -1;
            }
        }

        public int ListeningPort
        {
            get
            {
                return mPort > 0 ? mPort : 0;
            }
        }

        public bool Start()
        {
            return Start(0);
        }

        public bool Start(int port)
        {
            Stop();

            mPort = port;
            mSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            mSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, 1);

            if (mPort == 0)
            {
                return true;
            }

            try
            {
                mSocket.Bind(new IPEndPoint(IPAddress.Any, mPort));
                mSocket.BeginReceiveFrom(mTempCache, 0, mTempCache.Length, SocketFlags.None, ref mEndPoint, OnReceive, null);
            }
            catch (Exception exception)
            {
                Error(exception.Message);
                Stop();

                return false;
            }

            return true;
        }

        public void Stop()
        {
            mPort = -1;

            if (mSocket != null)
            {
                mSocket.Close();
                mSocket = null;
            }

            CarBuffer.Recycle(mInQueue);
            CarBuffer.Recycle(mOutQueue);
        }

        private void OnReceive(IAsyncResult result)
        {
            if (!IsActive)
            {
                return;
            }

            int bytes = 0;

            try
            {
                bytes = mSocket.EndReceiveFrom(result, ref mEndPoint);
            }
            catch (Exception exception)
            {
                Error(exception.Message);
            }

            if (bytes > 4)
            {
                CarBuffer buffer = CarBuffer.Create();
                buffer.BeginWriting(false).Write(mTempCache, 0, bytes);
                buffer.Read(4);

                CarDatagram datagram = new CarDatagram();
                datagram.buffer = buffer;
                datagram.ip = (IPEndPoint)mEndPoint;

                lock (mInQueue)
                {
                    mInQueue.Enqueue(datagram);
                }
            }

            if (mSocket != null)
            {
                mSocket.BeginReceiveFrom(mTempCache, 0, mTempCache.Length, SocketFlags.None, ref mEndPoint, OnReceive, null);
            }
        }

        public bool ReceivePacket(out CarBuffer buffer, out IPEndPoint source)
        {
            if (mPort == 0)
            {
                Stop();
                Error("You must specify a non-zero port to UdpProtocol.Start() before you can receive data.");
            }
            else if (mInQueue.Count != 0)
            {
                lock (mInQueue)
                {
                    CarDatagram datagram = mInQueue.Dequeue();
                    buffer = datagram.buffer;
                    source = datagram.ip;

                    return true;
                }
            }

            buffer = null;
            source = null;

            return false;
        }

        public void Broadcast(CarBuffer buffer, int port)
        {
            if (buffer != null)
            {
                buffer.MarkAsUsed();
                mBroadcastIP.Port = port;

                try
                {
                    mSocket.SendTo(buffer.StreamBuffer, buffer.Position, buffer.Size, SocketFlags.None, mBroadcastIP);
                }
                catch (Exception exception)
                {
                    Error(exception.Message);
                }

                buffer.Recycle();
            }
        }

        public void Send(CarBuffer buffer, IPEndPoint ip)
        {
            if (ip.Address.Equals(IPAddress.Broadcast))
            {
                Broadcast(buffer, ip.Port);

                return;
            }

            buffer.MarkAsUsed();

            if (mSocket != null)
            {
                buffer.Read();

                lock (mOutQueue)
                {
                    CarDatagram datagram = new CarDatagram();
                    datagram.buffer = buffer;
                    datagram.ip = ip;
                    mOutQueue.Enqueue(datagram);

                    if (mOutQueue.Count == 1)
                    {
                        mSocket.BeginSendTo(buffer.StreamBuffer, buffer.Position, buffer.Size, SocketFlags.None, ip, OnSend, null);
                    }
                }
            }
            else
            {
                buffer.Recycle();
                Error("The socket is null. Did you forget to call UdpProtocol.Start()?");
            }
        }

        private void OnSend(IAsyncResult result)
        {
            if (!IsActive)
            {
                return;
            }

            int bytes = 0;

            try
            {
                bytes = mSocket.EndSendTo(result);
            }
            catch (Exception exception)
            {
                bytes = 1;

                Error(exception.Message);
            }

            lock (mOutQueue)
            {
                mOutQueue.Dequeue().buffer.Recycle();

                if (bytes > 0 && mSocket != null && mOutQueue.Count != 0)
                {
                    CarDatagram datagram = mOutQueue.Peek();
                    mSocket.BeginSendTo(datagram.buffer.StreamBuffer, datagram.buffer.Position, datagram.buffer.Size, SocketFlags.None, datagram.ip, OnSend, null);
                }
            }
        }

        private void Error(string message)
        {
            if (OnErrorEvent != null)
            {
                OnErrorEvent(message);
            }
        }
    }
}