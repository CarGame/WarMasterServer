using System;
using System.IO;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Threading;
using System.Net;

namespace CarNetwork
{
    public class CarTcpServer
    {
        public delegate void OnCustomPacket(CarTcpProtocol protocol, CarBuffer buffer, BinaryReader reader, int packet);
        public delegate void OnTcpEndPointAction(CarTcpProtocol protocol);

        public event Action<string> OnErrorEvent;
        public event OnTcpEndPointAction OnEndPointConnectedEvent;
        public event OnTcpEndPointAction OnEndPointDisconnectEvent;
        public event OnTcpEndPointAction OnEndPointTimeoutEvent;

        private BetterList<CarTcpProtocol> mTcpProtocolList = new BetterList<CarTcpProtocol>();
        private Dictionary<int, OnCustomPacket> mMessageHandler = new Dictionary<int, OnCustomPacket>();

        private CarBuffer mBuffer;
        private TcpListener mListener;
        private Thread mThread;
        private long mTime;
        private int mListenerPort;
        private int mTimeoutTime = 30000;

        private const int CountPerSecond = 100;
        private const int ticksPerMillisecond = 10000;
        private const int Backlog = 512;

        public bool IsActive
        {
            get
            {
                return mThread != null;
            }
        }

        public bool IsListening
        {
            get
            {
                return (mListener != null);
            }
        }

        public int TcpPort
        {
            get
            {
                return (mListener != null) ? mListenerPort : 0;
            }
        }

        public int PlayerCount
        {
            get
            {
                return IsActive ? mTcpProtocolList.size : 0;
            }
        }

        public int TimeoutTime
        {
            get
            {
                return mTimeoutTime;
            }
            set
            {
                mTimeoutTime = value;
            }
        }

        private void ThreadFunction()
        {
            while (true)
            {
                CarBuffer buffer;
                bool processed = false;
                mTime = DateTime.Now.Ticks / ticksPerMillisecond;

                while (mListener != null && mListener.Pending())
                {
                    CarTcpProtocol tcpProtocol = AddTcpProtocol(mListener.AcceptSocket());
                    tcpProtocol.currentStage = CarTcpProtocol.Stage.Connected;

                    if (OnEndPointConnectedEvent != null)
                    {
                        OnEndPointConnectedEvent(tcpProtocol);
                    }
                }

                for (int i = 0; i < mTcpProtocolList.size; ++i)
                {
                    CarTcpProtocol protocol = mTcpProtocolList[i];

                    for (int j = 0; j < CountPerSecond && protocol.ReceivePacket(out buffer); ++j)
                    {
                        if (buffer.Size > 0)
                        {
                            try
                            {
                                if (ProcessPacket(buffer, protocol))
                                {
                                    processed = true;
                                }
                            }
                            catch (Exception exception)
                            {
                                if (OnEndPointDisconnectEvent != null)
                                {
                                    OnEndPointDisconnectEvent(protocol);
                                }

                                Error("ERROR (ProcessPlayerPacket): " + exception.Message);
                                RemoveProtocol(protocol);
                            }
                        }
                        buffer.Recycle();
                    }

                    if (mTimeoutTime > 0 && protocol.lastReceivedTime + mTimeoutTime < mTime)
                    {
                        if (OnEndPointTimeoutEvent != null)
                        {
                            OnEndPointTimeoutEvent(protocol);
                        }

                        Error(protocol.Address + " has timed out");
                        RemoveProtocol(protocol);
                        continue;
                    }
                }

                if (!processed)
                {
                    Thread.Sleep(1);
                }
            }
        }

        private BinaryWriter BeginSend(int type)
        {
            mBuffer = CarBuffer.Create();
            BinaryWriter writer = mBuffer.BeginPacket(type);
            return writer;
        }

        private void EndSend(IPEndPoint ip)
        {
            mBuffer.EndPacket();
            mBuffer.Recycle();
            mBuffer = null;
        }

        private void EndSend(CarTcpProtocol protocol)
        {
            mBuffer.EndPacket();
            protocol.SendTcpPacket(mBuffer);
            mBuffer.Recycle();
            mBuffer = null;
        }

        private bool ProcessPacket(CarBuffer buffer, CarTcpProtocol protocol)
        {
            BinaryReader reader = buffer.Read();
            int request = (int)reader.ReadInt32();

            if (mMessageHandler.ContainsKey(request))
            {
                if (mMessageHandler[request] != null)
                {
                    mMessageHandler[request](protocol, buffer, reader, request);
                    return true;
                }
            }

            return false;
        }

        public bool Start(int tcpPort)
        {
            Stop();

            try
            {
                mListenerPort = tcpPort;
                mListener = new TcpListener(IPAddress.Any, tcpPort);
                mListener.Start(Backlog);
            }
            catch (Exception exception)
            {
                Error(exception.Message);
                return false;
            }

            mThread = new Thread(ThreadFunction);
            mThread.Start();

            return true;
        }

        public void Stop()
        {
            if (mThread != null)
            {
                mThread.Abort();
                mThread = null;
            }

            if (mListener != null)
            {
                mListener.Stop();
                mListener = null;
            }

            for (int i = mTcpProtocolList.size - 1; i >= 0; --i)
            {
                if (OnEndPointDisconnectEvent != null)
                {
                    OnEndPointDisconnectEvent(mTcpProtocolList[i]);
                }

                RemoveProtocol(mTcpProtocolList[i]);
            }
        }

        private void Error(string message)
        {
            if (OnErrorEvent != null)
            {
                OnErrorEvent(message);
            }
        }

        private CarTcpProtocol AddTcpProtocol(Socket socket)
        {
            CarTcpProtocol tcpProtocol = new CarTcpProtocol();
            tcpProtocol.StartReceiving(socket);
            mTcpProtocolList.Add(tcpProtocol);

            return tcpProtocol;
        }

        private void RemoveProtocol(CarTcpProtocol protocol)
        {
            if (protocol != null)
            {
                protocol.Release();
                mTcpProtocolList.Remove(protocol);
            }
        }

        public void RegisterHandler(int packet, OnCustomPacket onCustomPacket)
        {
            if (mMessageHandler != null)
            {
                mMessageHandler.Add(packet, onCustomPacket);
            }
        }

        public void UnregisterHandler(int packet)
        {
            if (mMessageHandler != null && mMessageHandler.ContainsKey(packet))
            {
                mMessageHandler.Remove(packet);
            }
        }
    }
}