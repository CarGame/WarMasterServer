﻿using System.Collections.Generic;

public class CarGrammarAssist
{
    public static List<float> SplitFloatList(string content, char separator)
    {
        if (!string.IsNullOrEmpty(content))
        {
            List<float> itemList = new List<float>();

            string[] itemArray = content.Split(separator);
            if (itemArray != null)
            {
                int itemCount = itemArray.Length;
                for (int i = 0; i < itemCount; ++i)
                {
                    float itemValue = float.Parse(itemArray[i]);
                    itemList.Add(itemValue);
                }
            }

            return itemList;
        }

        return null;
    }

    public static List<int> SplitIntList(string content, char separator)
    {
        if (!string.IsNullOrEmpty(content))
        {
            List<int> itemList = new List<int>();

            string[] itemArray = content.Split(separator);
            if (itemArray != null)
            {
                int itemCount = itemArray.Length;
                for (int i = 0; i < itemCount; ++i)
                {
                    int itemValue = int.Parse(itemArray[i]);
                    itemList.Add(itemValue);
                }
            }

            return itemList;
        }

        return null;
    }

    public static bool CheckArrayBound<T>(T[] itemArray, int index)
    {
        if (itemArray != null && index >= 0 && index <= itemArray.Length - 1)
        {
            return true;
        }

        return false;
    }

    public static bool CheckListBound<T>(List<T> itemList, int index)
    {
        if (itemList != null && index >= 0 && index <= itemList.Count - 1)
        {
            return true;
        }

        return false;
    }

}
