﻿using System.Collections.Generic;

public class BattleManager
{
    private Dictionary<int, Battle> mBattleDictionary = new Dictionary<int, Battle>();

    public void AddBattle(int battleId, Battle battle)
    {
        mBattleDictionary.Add(battleId, battle);
    }

    public Battle GetBattle(int battleId)
    {
        if (mBattleDictionary.ContainsKey(battleId))
        {
            return mBattleDictionary[battleId];
        }
        else
        {
            return null;
        }
    }

    public void RemoveBattle(int battleId)
    {
        if (mBattleDictionary.ContainsKey(battleId))
        {
            mBattleDictionary.Remove(battleId);
        }
    }

}

