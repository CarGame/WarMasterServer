﻿using CarNetwork;
using GameProtocol;
using MySql.Data.MySqlClient;
using System.Collections.Generic;

public class RoleSystem : GameSystem
{
    private int mDefaultLevel = 1;
    private int mDefaultExperience = 0;

    public override void Awake()
    {

    }

    public override void Start()
    {
        MessageHandler.Instance.OnGetRoleInfoRequestEvent += HandleGetRoleInfoRequest;
    }

    public override void Update()
    {

    }

    public override void Destroy()
    {
        MessageHandler.Instance.OnGetRoleInfoRequestEvent -= HandleGetRoleInfoRequest;
    }

    public void HandleGetRoleInfoRequest(CarTcpProtocol tcpProtocol, GetRoleInfoRequest getRoleInfoRequest)
    {
        GamePlayer gamePlayer = GameDataManager.Instance.gamePlayerManager.GetGamePlayerByIp(tcpProtocol);
        if (gamePlayer != null)
        {
            string commandTextFormat = "select * from account.role where accountId = '{0}'";
            string commandText = string.Format(commandTextFormat, gamePlayer.accountId);
            MySqlDataReader reader = MySqlAccessor.Instance.BeginExecuteReader(commandText);
            RoleInfoMessage roleInfo = null;

            if (reader.Read())
            {
                roleInfo = new RoleInfoMessage();
                roleInfo.roleId = reader.GetInt64((int)DatabaseTableHeader.RoleTableHeaderEnum.id);
                roleInfo.name = reader.GetString((int)DatabaseTableHeader.RoleTableHeaderEnum.name);
                roleInfo.level = reader.GetInt32((int)DatabaseTableHeader.RoleTableHeaderEnum.level);
                roleInfo.experience = reader.GetInt32((int)DatabaseTableHeader.RoleTableHeaderEnum.experience);
            }
            MySqlAccessor.Instance.EndExecuteReader(reader);

            if (roleInfo == null)
            {
                commandTextFormat = "insert account.role(accountId, name, level, experience) values (?accountId, ?name, ?level , ?experience);select @@identity";
                List<MySqlParameter> insertParameterList = new List<MySqlParameter>();
                insertParameterList.Add(new MySqlParameter("?accountId", gamePlayer.accountId));
                insertParameterList.Add(new MySqlParameter("?name", gamePlayer.accountId.ToString()));
                insertParameterList.Add(new MySqlParameter("?level", mDefaultLevel));
                insertParameterList.Add(new MySqlParameter("?experience", mDefaultExperience));

                long roleId = (long)MySqlAccessor.Instance.ExecuteNoQuery(commandTextFormat, insertParameterList.ToArray());

                roleInfo = new RoleInfoMessage();
                roleInfo.roleId = roleId;
                roleInfo.name = gamePlayer.accountId.ToString();
                roleInfo.level = mDefaultLevel;
            }

            gamePlayer.roleId = roleInfo.roleId;
            gamePlayer.actor.SetName(roleInfo.name);
            gamePlayer.actor.SetLevel(roleInfo.level);
            gamePlayer.actor.SetExperience(roleInfo.experience);

            MessageSender.Instance.SendGetRoleInfoResponse(tcpProtocol, roleInfo);
        }
    }
}