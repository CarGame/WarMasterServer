﻿using CarSwind;
using System;

public class GameHallSystem : GameSystem
{
    public override void Awake()
    {
        MessageHandler.Instance.OnSingleMatchEvent += HandleSingleMatch;
    }

    public override void Start()
    {

    }

    public override void Update()
    {

    }

    public override void Destroy()
    {
        MessageHandler.Instance.OnSingleMatchEvent -= HandleSingleMatch;
    }

    private void HandleSingleMatch(CarTcpPlayer player)
    {
        if (player != null)
        {
            GamePlayer gamePlayer = GameDataManager.Instance.gamePlayerManager.GetPlayer(player.accountID);
            MatchSystem.Instance.Match(MatchType.OneOne, gamePlayer);
        }
    }

}

