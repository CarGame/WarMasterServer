﻿using CarSwind;
using System;
using System.Collections.Generic;

public class MatchSystem : Singleton<MatchSystem>
{
    public int fightingCapacityTolerance = 5;

    private List<MatchItem> mOneOneMatchItemList = new List<MatchItem>();

    public void Match(MatchType matchType, GamePlayer gamePlayer)
    {
        int matchItemCount = mOneOneMatchItemList.Count;
        for (int i = matchItemCount - 1; i >= 0; i--)
        {
            if (mOneOneMatchItemList[i] != null && mOneOneMatchItemList[i].fightingCapacity - gamePlayer.roleInfo.level <= fightingCapacityTolerance)
            {
                MessageSender.Instance.SendSingleMatchResponse(gamePlayer.carTcpPlayer);
                MessageSender.Instance.SendSingleMatchResponse(mOneOneMatchItemList[i].gamePlayer.carTcpPlayer);
            }
        }

        if (gamePlayer != null)
        {
            MatchItem matchItem = new MatchItem();
            matchItem.matchType = matchType;
            matchItem.gamePlayer = gamePlayer;

            mOneOneMatchItemList.Add(matchItem);
        }
    }

    public void CancelMatch(GamePlayer gamePlayer)
    {
        int matchItemCount = mOneOneMatchItemList.Count;
        for (int i = matchItemCount - 1; i >= 0; --i)
        {
            if (mOneOneMatchItemList[i] != null && mOneOneMatchItemList[i].gamePlayer == gamePlayer)
            {
                mOneOneMatchItemList.RemoveAt(i);
                break;
            }
        }
    }
}

