﻿using System;
using System.Collections.Generic;

public enum MatchType
{
    OneOne,//1 vs 1 , one person one hero
}

public class MatchItem
{
    public MatchType matchType = MatchType.OneOne;

    public int fightingCapacity;
    public GamePlayer gamePlayer;
}

public class MatchGroup
{
    public MatchType matchType = MatchType.OneOne;

    public int fightingCapacity;
    public List<MatchItem> matchItemList = new List<MatchItem>();
}

