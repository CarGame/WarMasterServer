﻿
public class GameSystemManager
{
    private CommonSystem mCommonSystem = new CommonSystem();
    private AccountSystem mAccountSystem = new AccountSystem();
    private RoleSystem mRoleSystem = new RoleSystem();
    private BattleSystem mBattleSystem = new BattleSystem();

    public void Awake()
    {
        mCommonSystem.Awake();
        mAccountSystem.Awake();
        mRoleSystem.Awake();
        mBattleSystem.Awake();
    }

    public void Start()
    {
        mCommonSystem.Start();
        mAccountSystem.Start();
        mRoleSystem.Start();
        mBattleSystem.Start();
    }

    public void Update()
    {

    }

    public void Destroy()
    {
        mCommonSystem.Destroy();
        mAccountSystem.Destroy();
        mRoleSystem.Destroy();
        mBattleSystem.Destroy();
    }
}