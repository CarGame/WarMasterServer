﻿using CarNetwork;
using GameProtocol;
using System.Collections.Generic;
using UnityEngine;

public class GamePlayer
{
    public CarTcpProtocol tcpProtocol;
    public long accountId;
    public long roleId;
    public Actor actor = new Actor();

    public void OnAwake()
    {

    }

    public void OnStart()
    {

    }

    public void OnDestroy()
    {

    }

    public void OnUpdate()
    {

    }

    public void OnConnected()
    {

    }

    public void OnReconnected()
    { 

    }

    public void OnDisconnected()
    {

    }

    public void OnKickOut()
    {

    }
}

