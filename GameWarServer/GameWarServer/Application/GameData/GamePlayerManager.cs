﻿using CarNetwork;
using System.Collections.Generic;

public class GamePlayerManager
{
    //connected GamePlayer
    private Dictionary<CarTcpProtocol, GamePlayer> mGamePlayerDictionary = new Dictionary<CarTcpProtocol, GamePlayer>();

    //logined GamePlayer
    private Dictionary<long, GamePlayer> mGamePlayerIndexDictionary = new Dictionary<long, GamePlayer>();

    public GamePlayer AddGamePlayerByIp(CarTcpProtocol tcpProtocol)
    {
        if (tcpProtocol != null)
        {
            if (mGamePlayerDictionary.ContainsKey(tcpProtocol))
            {
                return mGamePlayerDictionary[tcpProtocol];
            }

            GamePlayer gamePlayer = new GamePlayer();
            gamePlayer.tcpProtocol = tcpProtocol;
            gamePlayer.OnAwake();
            gamePlayer.OnStart();
            gamePlayer.OnConnected();

            mGamePlayerDictionary.Add(tcpProtocol, gamePlayer);

            return gamePlayer;
        }

        return null;
    }

    public GamePlayer AddGamePlayerId(CarTcpProtocol tcpProtocol, long accountId)
    {
        if (tcpProtocol != null)
        {
            if (mGamePlayerIndexDictionary.ContainsKey(accountId))
            {
                if (mGamePlayerIndexDictionary[accountId] != null)
                {
                    mGamePlayerIndexDictionary[accountId].OnKickOut();
                    mGamePlayerDictionary.Remove(mGamePlayerIndexDictionary[accountId].tcpProtocol);
                    mGamePlayerDictionary[tcpProtocol] = mGamePlayerIndexDictionary[accountId];

                    mGamePlayerIndexDictionary[accountId].tcpProtocol = tcpProtocol;
                    mGamePlayerIndexDictionary[accountId].accountId = accountId;
                    mGamePlayerIndexDictionary[accountId].OnAwake();
                    mGamePlayerIndexDictionary[accountId].OnStart();
                    mGamePlayerIndexDictionary[accountId].OnReconnected();

                    return mGamePlayerIndexDictionary[accountId];
                }
            }
            else
            {
                if (mGamePlayerDictionary[tcpProtocol] != null)
                {
                    mGamePlayerDictionary[tcpProtocol].accountId = accountId;
                }

                if (!mGamePlayerIndexDictionary.ContainsKey(accountId))
                {
                    mGamePlayerIndexDictionary.Add(accountId, mGamePlayerDictionary[tcpProtocol]);
                }

                return mGamePlayerDictionary[tcpProtocol];
            }
        }

        return null;
    }

    public void RemoveGamePlayer(GamePlayer gamePlayer)
    {
        if (gamePlayer != null)
        {
            RemoveGamePlayerByIp(gamePlayer.tcpProtocol);
        }
    }

    public bool RemoveGamePlayerByIp(CarTcpProtocol tcpProtocol)
    {
        if (tcpProtocol != null && mGamePlayerDictionary.ContainsKey(tcpProtocol))
        {
            mGamePlayerIndexDictionary.Remove(mGamePlayerDictionary[tcpProtocol].accountId);
            mGamePlayerDictionary.Remove(tcpProtocol);

            return true;
        }

        return false;
    }

    public GamePlayer GetGamePlayerByIp(CarTcpProtocol tcpProtocol)
    {
        GamePlayer gamePlayer = null;
        mGamePlayerDictionary.TryGetValue(tcpProtocol, out gamePlayer);

        return gamePlayer;
    }

    public GamePlayer GetGamePlayerById(long accountId)
    {
        GamePlayer gamePlayer = null;
        mGamePlayerIndexDictionary.TryGetValue(accountId, out gamePlayer);

        return gamePlayer;
    }

    public List<GamePlayer> GetGamePlayerList()
    {
        List<GamePlayer> gamePlayerList = new List<GamePlayer>();

        foreach (KeyValuePair<CarTcpProtocol, GamePlayer> gamePlayerEntry in mGamePlayerDictionary)
        {
            if (gamePlayerEntry.Value != null)
            {
                gamePlayerList.Add(gamePlayerEntry.Value);
            }
        }

        return gamePlayerList;
    }
}