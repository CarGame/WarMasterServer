﻿
public class SoundTable
{
    public enum SoundHeader
    {
        Id,
        SoundName,
        PathFilename,
        SoundType,
        PlayCount,
        PlayDelayTime,
        PlayFadeinTime,
        PlayFadeoutTime,
        StartVolume,
    }

    public class SoundEntry
    {
        public int id;
        public string soundName;
        public string pathFilename;
        public int soundType;
        public int playCount;
        public float playDelayTime;
        public float playFadeinTime;
        public float playFadeoutTime;
        public float startVolume;
    }

    public const string filename = "Sound";

    public static SoundEntry GetEntry(int id)
    {
        SoundEntry soundEntry = new SoundEntry();
        soundEntry.id = id;
        soundEntry.soundName = GameDocument.Instance.GetCellContent(filename, id, (int)SoundHeader.SoundName);
        soundEntry.pathFilename = GameDocument.Instance.GetCellContent(filename, id, (int)SoundHeader.PathFilename);
        soundEntry.soundType = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)SoundHeader.SoundType));
        soundEntry.playCount = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)SoundHeader.PlayCount));
        soundEntry.playDelayTime = float.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)SoundHeader.PlayDelayTime));
        soundEntry.playFadeinTime = float.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)SoundHeader.PlayFadeinTime));
        soundEntry.playFadeoutTime = float.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)SoundHeader.PlayFadeoutTime));
        soundEntry.startVolume = float.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)SoundHeader.StartVolume));

        return soundEntry;
    }
}
