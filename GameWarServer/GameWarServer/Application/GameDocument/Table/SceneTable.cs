﻿public class SceneTable
{
    public enum SceneHeader
    {
        Id,
        SceneName,
        ScenePathFilename,
    }

    public class SceneEntry
    {
        public int id;
        public string sceneName;
        public string scenePathFilename;
    }

    public const string filename = "Scene";

    public static SceneEntry GetSceneEntry(int id)
    {
        SceneEntry sceneEntry = new SceneEntry();
        sceneEntry.id = id;
        sceneEntry.sceneName = GameDocument.Instance.GetCellContent(filename, id, (int)SceneHeader.SceneName);
        sceneEntry.scenePathFilename = GameDocument.Instance.GetCellContent(filename, id, (int)SceneHeader.ScenePathFilename);

        return sceneEntry;
    }
}
