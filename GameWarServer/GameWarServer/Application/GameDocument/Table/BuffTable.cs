﻿using System.Collections.Generic;

public class BuffTable
{
    public enum BuffHeader
    {
        Id,
        Name,
        Type,
        LastCondition,
        PropertyType,
        BuffValueType,
        BuffValue,
        LifeTime,
        TriggerDelay,
        TriggerInterval,
        MaxTriggerCount,
        MaxHoldCount,
        Priority,
        IsProfitable,
        MutexBuffIdArray,
        EffectPathFilename,
    }

    public class BuffEntry
    {
        public int id;
        public string name;
        public int type;
        public int lastCondition;
        public int propertyType;
        public int buffValueType;
        public float buffValue;
        public float lifeTime;
        public float triggerDelay;
        public float triggerInterval;
        public int maxTriggerCount;
        public int maxHoldCount;
        public int priority;
        public bool isProfitable;
        public List<int> mutexBuffIdList;
        public string effectPathFilename;
    }

    public const string filename = "Buff";

    public static BuffEntry GetEntry(int id)
    {
        BuffEntry buffEntry = new BuffEntry();
        buffEntry.id = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)BuffHeader.Id));
        buffEntry.name = GameDocument.Instance.GetCellContent(filename, id, (int)BuffHeader.Name);
        buffEntry.type = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)BuffHeader.Type));
        buffEntry.lastCondition = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)BuffHeader.LastCondition));
        buffEntry.propertyType = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)BuffHeader.PropertyType));
        buffEntry.buffValueType = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)BuffHeader.BuffValueType));
        buffEntry.buffValue = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)BuffHeader.BuffValue));
        buffEntry.lifeTime = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)BuffHeader.LifeTime));
        buffEntry.triggerDelay = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)BuffHeader.TriggerDelay));
        buffEntry.triggerInterval = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)BuffHeader.TriggerInterval));
        buffEntry.maxTriggerCount = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)BuffHeader.MaxTriggerCount));
        buffEntry.maxHoldCount = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)BuffHeader.MaxHoldCount));
        buffEntry.priority = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)BuffHeader.Priority));
        buffEntry.isProfitable = bool.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)BuffHeader.IsProfitable));
        buffEntry.mutexBuffIdList = CarGrammarAssist.SplitIntList(GameDocument.Instance.GetCellContent(filename, id, (int)BuffHeader.MutexBuffIdArray), ',');
        buffEntry.effectPathFilename = GameDocument.Instance.GetCellContent(filename, id, (int)BuffHeader.EffectPathFilename);

        return buffEntry;
    }
}
