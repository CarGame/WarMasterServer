﻿public class BulletTable
{
    public enum BulletHeader
    {
        Id,
        AreaType,
        TrackType,
        AttackMode,
        Speed,
        LifeTime,
        AttackInterval,
        ResourcePathFilename,
    }


    public class BulletEntry
    {
        public int id;
        public int areaType;
        public int trackType;
        public int attackMode;
        public float speed;
        public float lifeTime;
        public float attackInterval;
        public string resourcePathFilename;
    }

    public const string filename = "Bullet";

    public static BulletEntry GetEntry(int id)
    {
        BulletEntry bulletEntry = new BulletEntry();
        bulletEntry.id = id;
        bulletEntry.areaType = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)BulletHeader.AreaType));
        bulletEntry.trackType = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)BulletHeader.TrackType));
        bulletEntry.attackMode = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)BulletHeader.AttackMode));
        bulletEntry.speed = float.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)BulletHeader.Speed));
        bulletEntry.lifeTime = float.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)BulletHeader.LifeTime));
        bulletEntry.attackInterval = float.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)BulletHeader.AttackInterval));
        bulletEntry.resourcePathFilename = GameDocument.Instance.GetCellContent(filename, id, (int)BulletHeader.ResourcePathFilename);

        return bulletEntry;
    }
}
