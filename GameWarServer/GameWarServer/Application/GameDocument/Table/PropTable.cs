﻿public class PropTable
{
    public enum PropHeader
    {
        Id,
        Name,
        ResourcePathFilename,
    }

    public class PropEntry
    {
        public int id;
        public string name;
        public string pathFilename;
    }

    public const string filename = "Prop";

    public static PropEntry GetEntry(int id)
    {
        PropEntry propEntry = new PropEntry();
        propEntry.id = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)PropHeader.Id));
        propEntry.name = GameDocument.Instance.GetCellContent(filename, id, (int)PropHeader.Name);
        propEntry.pathFilename = GameDocument.Instance.GetCellContent(filename, id, (int)PropHeader.ResourcePathFilename);

        return propEntry;
    }
}
