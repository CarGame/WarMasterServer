﻿
public class ActorTable
{
    public enum ActorHeader
    {
        Id,
        ActorName,
        ModelId,
        Element,
        ActorValueId,
        AttackType,
        IdleSegmentCount,
        AttackSegmentCount,
    }

    public class ActorEntry
    {
        public int id;
        public string actorName;
        public int modelId;
        public int element;
        public int actorValueId;
        public int attackType;
        public int idleSegmentCount;
        public int attackSegmentCount;
    }

    public const string filename = "Actor";

    public static ActorEntry GetEntry(int id)
    {
        ActorEntry actorEntry = new ActorEntry();
        actorEntry.id = id;
        actorEntry.actorName = GameDocument.Instance.GetCellContent(filename, id, (int)ActorHeader.ActorName);
        actorEntry.modelId = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorHeader.ModelId));
        actorEntry.element = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorHeader.Element));
        actorEntry.actorValueId = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorHeader.ActorValueId));
        actorEntry.attackType = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorHeader.AttackType));
        actorEntry.idleSegmentCount = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorHeader.IdleSegmentCount));
        actorEntry.attackSegmentCount = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorHeader.AttackSegmentCount));

        return actorEntry;
    }
}
