﻿using System.Collections.Generic;
using System.IO;

public class GameDocument : Singleton<GameDocument>
{
    private Dictionary<string, CJFFile> cjfFileDictionary = new Dictionary<string, CJFFile>();

    private const string documentPath = "Document/TXT";
    private const string documentSearchPattern = "*.txt";

    public void StartDocuments()
    {
        string[] filePathFilenames = Directory.GetFiles(documentPath, documentSearchPattern, SearchOption.AllDirectories);

        int length = filePathFilenames.Length;
        for (int i = 0; i < length; ++i)
        {
            string filename = Path.GetFileNameWithoutExtension(filePathFilenames[i]);
            string fileContent = CJFFile.LoadFile(filePathFilenames[i]);

            Add(filename, fileContent);
        }
    }

    public void Add(string filename, string fileContent)
    {
        if (!cjfFileDictionary.ContainsKey(filename))
        {
            CJFFile cjfFile = CJFFile.Create(fileContent);
            cjfFileDictionary.Add(filename, cjfFile);
        }
    }

    public string GetCellContent(string filename, int id, int column)
    {
        if (cjfFileDictionary.ContainsKey(filename))
        {
            string cellContent = cjfFileDictionary[filename].GetCellContent(id, column);

            return cellContent;
        }

        return string.Empty;
    }

    public CJFFile GetCJFFile(string filename)
    {
        if (cjfFileDictionary.ContainsKey(filename))
        {
            return cjfFileDictionary[filename];
        }

        return null;
    }
}