﻿using GameProtocol;
using System.Collections.Generic;
using UnityEngine;

public class Actor
{
    public TransformController transformController = new TransformController();
    public PropertyController propertyController = new PropertyController();

    private string mName;
    private int mLevel;
    private int mExperience;

    public Vector3 Position
    {
        get
        {
            return transformController.Position;
        }
        set
        {
            transformController.Position = value;
        }
    }

    public Vector3 EulerAngles
    {
        get
        {
            return transformController.EulerAngles;
        }
        set
        {
            transformController.EulerAngles = value;
        }
    }

    public Vector3 LocalScale
    {
        get
        {
            return transformController.LocalScale;
        }
        set
        {
            transformController.LocalScale = value;
        }
    }

    public string Name
    {
        get
        {
            return mName;
        }
    }

    public int Level
    {
        get
        {
            return mLevel;
        }
    }

    public int Experience
    {
        get
        {
            return mExperience;
        }
    }

    public void SetName(string name)
    {
        mName = name;
    }

    public void SetLevel(int level)
    {
        mLevel = level;
    }

    public void SetExperience(int experience)
    {
        mExperience = experience;
    }

    public List<PropertyItem> GetPropertyItemList()
    {
        List<PropertyItem> propertyItemList = new List<PropertyItem>();

        int endIndex = (int)ActorPropertyType.max;
        for (int i = 0; i < endIndex; ++i)
        {
            int propertyType = i;
            float propertyValue = propertyController.GetPropertyValue((ActorPropertyType)i);

            PropertyItem propertyItem = new PropertyItem();
            propertyItem.propertyType = (ActorPropertyType)propertyType;
            propertyItem.propertyValue = propertyValue;

            propertyItemList.Add(propertyItem);
        }

        return propertyItemList;
    }
}
