﻿using UnityEngine;

public class TransformController
{
    private Vector3 mPosition = Vector3.zero;
    private Vector3 mEulerAngles = Vector3.zero;
    private Vector3 mLocalScale = Vector3.zero;

    public Vector3 Position
    {
        get
        {
            return mPosition;
        }
        set
        {
            mPosition = value;
        }
    }

    public Vector3 EulerAngles
    {
        get
        {
            return mEulerAngles;
        }
        set
        {
            mEulerAngles = value;
        }
    }

    public Vector3 LocalScale
    {
        get
        {
            return mLocalScale;
        }
        set
        {
            mLocalScale = value;
        }
    }
}

