﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;
using System.IO;

namespace CarServerTest
{
    [TestClass]
    public class GameDocumentTest
    {
        [TestMethod]
        public void StartDocumentsTest()
        {
            string[] filePathFilenames = Directory.GetFiles("Document/TXT", "*.txt", SearchOption.AllDirectories);

            int length = filePathFilenames.Length;
            for (int i = 0; i < length; ++i)
            {
                StreamReader reader = new StreamReader(filePathFilenames[i]);
                string fileContent = reader.ReadToEnd();
                reader.Close();
                reader.Dispose();

                Debug.WriteLine(fileContent);
            }
        }

    }

}
