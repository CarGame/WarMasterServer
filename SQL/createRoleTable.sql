use account;

CREATE TABLE role
(
	id bigint not null auto_increment,
	accountId varchar(45) BINARY not null,
	name varchar(255) not null,
	level int not null,
	experience int not null default 1,
	PRIMARY KEY(id),
	UNIQUE(id),
	UNIQUE(name)
);
