use account;

CREATE TABLE account
(
	id bigint not null auto_increment,
	account varchar(45) BINARY not null,
	password varchar(45) not null,
	email varchar(45),
	mobile varchar(45),
	PRIMARY KEY(id),
	UNIQUE(account)
);
