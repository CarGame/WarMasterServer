echo on
protogen -i:Source/C2S_Message.proto -o:Target/Message/C2S_Message.cs
protogen -i:Source/S2C_Message.proto -o:Target/Message/S2C_Message.cs
protogen -i:Source/Common_Message.proto -o:Target/Message/Common_Message.cs
protogen -i:Source/Packet.proto -o:Target/Command/Packet.cs
pause