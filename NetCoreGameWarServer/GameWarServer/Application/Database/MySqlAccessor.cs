﻿using MySql.Data.MySqlClient;
using System;

public class MySqlAccessor : Singleton<MySqlAccessor>, IDisposable
{
    private MySqlConnection connection;
    private bool disposed;

    public void Start(string connectionString)
    {
        connection = new MySqlConnection();
        connection.ConnectionString = connectionString;
        connection.Open();
    }

    ~MySqlAccessor()
    {
        if (disposed == false)
        {
            Dispose();
        }
    }

    public MySqlDataReader BeginExecuteReader(string commandText)
    {
        MySqlDataReader reader = BeginExecuteReader(commandText, null);
        return reader;
    }

    public MySqlDataReader BeginExecuteReader(string commandText, MySqlParameter[] parameters)
    {
        MySqlDataReader reader = MySqlHelper.ExecuteReader(connection, commandText, parameters);
        return reader;
    }

    public void EndExecuteReader(MySqlDataReader reader)
    {
        reader.Close();
        reader.Dispose();
    }

    public int ExecuteNoQuery(string commandText)
    {
        int count = ExecuteNoQuery(commandText, null);
        return count;
    }

    public int ExecuteNoQuery(string commandText, MySqlParameter[] parameters)
    {
        int count = MySqlHelper.ExecuteNonQuery(connection, commandText, parameters);
        return count;
    }

    public object ExecuteScalar(string commandText)
    {
        object resultObject = ExecuteScalar(commandText, null);
        return resultObject;
    }

    public object ExecuteScalar(string commandText, MySqlParameter[] parameters)
    {
        object resultObject = MySqlHelper.ExecuteScalar(connection, commandText, parameters);
        return resultObject;
    }

    public void Close()
    {
        connection.Close();
    }

    public void Dispose()
    {
        connection.Dispose();
        disposed = true;
        GC.SuppressFinalize(this);
    }
}