﻿
public class DatabaseTableHeader
{
    public enum AccountTableHeaderEnum
    {
        id,
        account,
        password,
        email,
        mobile,
    }

    public enum RoleTableHeaderEnum
    {
        id,
        accountId,
        name,
        level,
        experience,
    }
}

