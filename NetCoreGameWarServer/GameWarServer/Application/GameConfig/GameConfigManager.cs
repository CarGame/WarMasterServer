﻿using System.IO;
using System;
using SimpleJson;

public class GameConfigManager : Singleton<GameConfigManager>
{
    public static readonly string Version = "version";
    public static readonly string TcpPort = "tcpPort";
    public static readonly string ConnectionString = "connectionString";
    public static readonly string TimeoutTime = "timeoutTime";

    public event Action OnLoadConfigFinishedEvent;

    private JsonObject gameConfigJsonObject;

    public void LoadConfigFile(string filePathName)
    {
        StreamReader reader = new StreamReader(filePathName);
        string fileContent = reader.ReadToEnd();
        gameConfigJsonObject = SimpleJson.SimpleJson.DeserializeObject(fileContent) as JsonObject;
        reader.Close();
        reader.Dispose();

        if (OnLoadConfigFinishedEvent != null)
        {
            OnLoadConfigFinishedEvent();
        }
    }

    public string GetValue(string keyName)
    {
        if (gameConfigJsonObject.ContainsKey(keyName))
        {
            return gameConfigJsonObject[keyName] as string;
        }
        else
        {
            return string.Empty;
        }
    }
}
