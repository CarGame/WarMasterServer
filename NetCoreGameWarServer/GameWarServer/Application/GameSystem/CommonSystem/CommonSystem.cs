﻿using CarNetwork;
using System;
using System.Collections.Generic;

public class CommonSystem : GameSystem
{
    public override void Awake()
    {

    }

    public override void Start()
    {
        GameNetwork.Instance.OnEndPointConnectedEvent += OnEndPointConnected;
        GameNetwork.Instance.OnEndPointDisconnectEvent += OnEndPointDisconnected;
        GameNetwork.Instance.OnEndPointTimeoutEvent += OnEndPointTimeout;

        MessageHandler.Instance.OnEmptyRequestEvent += HandleEmptyRequest;
    }

    public override void Update()
    {

    }

    public override void Destroy()
    {
        GameNetwork.Instance.OnEndPointConnectedEvent -= OnEndPointConnected;
        GameNetwork.Instance.OnEndPointDisconnectEvent -= OnEndPointDisconnected;
        GameNetwork.Instance.OnEndPointTimeoutEvent -= OnEndPointTimeout;

        MessageHandler.Instance.OnEmptyRequestEvent -= HandleEmptyRequest;
    }

    private void HandleEmptyRequest(CarTcpProtocol tcpProtocol)
    {
        MessageSender.Instance.SendEmptyResponse(tcpProtocol);
    }

    private void OnEndPointConnected(CarTcpProtocol tcpProtocol)
    {
        Console.WriteLine("OnPlayerConnected: " + tcpProtocol.Address);

        GameDataManager.Instance.gamePlayerManager.AddGamePlayerByIp(tcpProtocol);
    }

    private void OnEndPointTimeout(CarTcpProtocol tcpProtocol)
    {
        if (tcpProtocol != null)
        {
            Console.WriteLine("OnPlayerTimeout: " + tcpProtocol.Address);

            GamePlayer gamePlayer = GameDataManager.Instance.gamePlayerManager.GetGamePlayerByIp(tcpProtocol);
            if (gamePlayer != null)
            {
                gamePlayer.OnDisconnected();
                BroadcastDestroyActor(gamePlayer);
            }

            GameDataManager.Instance.gamePlayerManager.RemoveGamePlayerByIp(tcpProtocol);
        }
    }

    private void OnEndPointDisconnected(CarTcpProtocol tcpProtocol)
    {
        if (tcpProtocol != null)
        {
            Console.WriteLine("OnPlayerDisconnected: " + tcpProtocol.Address);
            GamePlayer gamePlayer = GameDataManager.Instance.gamePlayerManager.GetGamePlayerByIp(tcpProtocol);
            if (gamePlayer != null)
            {
                gamePlayer.OnDisconnected();
            }

            GameDataManager.Instance.gamePlayerManager.RemoveGamePlayerByIp(tcpProtocol);
        }
    }

    private void BroadcastDestroyActor(GamePlayer gamePlayer)
    {
        if (gamePlayer != null)
        {
            List<GamePlayer> gamePlayerList = GameDataManager.Instance.gamePlayerManager.GetGamePlayerList();
            if (gamePlayerList != null)
            {
                int gamePlayerCount = gamePlayerList.Count;
                for (int i = 0; i < gamePlayerCount; ++i)
                {
                    if (gamePlayerList[i] != null && gamePlayerList[i].tcpProtocol != null && gamePlayerList[i] != gamePlayer)
                    {
                        MessageSender.Instance.SendDestroyActorEvent(gamePlayerList[i].tcpProtocol, gamePlayer.accountId);
                    }
                }
            }
        }
    }
}
