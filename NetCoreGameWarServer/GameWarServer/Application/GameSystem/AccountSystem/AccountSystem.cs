﻿using CarNetwork;
using GameProtocol;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

public class AccountSystem : GameSystem
{
    public override void Awake()
    {

    }

    public override void Start()
    {
        MessageHandler.Instance.OnRegisterRequestEvent += HandleRegisterRequest;
        MessageHandler.Instance.OnLoginRequestEvent += HandleLoginRequest;
    }

    public override void Update()
    {

    }

    public override void Destroy()
    {
        MessageHandler.Instance.OnRegisterRequestEvent -= HandleRegisterRequest;
        MessageHandler.Instance.OnLoginRequestEvent -= HandleLoginRequest;
    }

    public void HandleRegisterRequest(CarTcpProtocol tcpProtocol, RegisterRequest registerRequest)
    {
        Console.WriteLine(registerRequest.account);
        Console.WriteLine(registerRequest.password);

        string searchCommandText = "select * from account.account where account = ?account and password = ?password";
        List<MySqlParameter> searchParameterList = new List<MySqlParameter>();
        searchParameterList.Add(new MySqlParameter("?account", registerRequest.account));
        searchParameterList.Add(new MySqlParameter("?password", registerRequest.password));
        object searchResultObject = MySqlAccessor.Instance.ExecuteScalar(searchCommandText, searchParameterList.ToArray());

        if (searchResultObject != null)
        {
            MessageSender.Instance.SendErrorEvent(tcpProtocol, (int)ErrorCodeEnum.AccountExist);
        }
        else
        {
            string insertCommandText = "insert account.account(account,password,email,mobile) values (?account,?password,?email,?mobile);select @@identity";
            List<MySqlParameter> insertParameterList = new List<MySqlParameter>();
            insertParameterList.Add(new MySqlParameter("?account", registerRequest.account));
            insertParameterList.Add(new MySqlParameter("?password", registerRequest.password));
            insertParameterList.Add(new MySqlParameter("?email", registerRequest.email));
            insertParameterList.Add(new MySqlParameter("?mobile", registerRequest.mobile));

            object resultObject = MySqlAccessor.Instance.ExecuteScalar(insertCommandText, insertParameterList.ToArray());
            if (resultObject != null)
            {
                long accountId = (long)resultObject;
                Console.WriteLine(accountId);

                GameDataManager.Instance.gamePlayerManager.AddGamePlayerId(tcpProtocol, accountId);

                MessageSender.Instance.SendRegisterResponse(tcpProtocol, accountId);
            }
        }
    }

    public void HandleLoginRequest(CarTcpProtocol tcpProtocol, LoginRequest loginRequest)
    {
        Console.WriteLine(loginRequest.account);
        Console.WriteLine(loginRequest.password);

        string commandTextFormat = "select * from account.account where account = '{0}' and password = '{1}'";
        string commandText = string.Format(commandTextFormat, loginRequest.account, loginRequest.password);
        object resultObject = MySqlAccessor.Instance.ExecuteScalar(commandText);
        if (resultObject != null)
        {
            long accountId = (long)resultObject;

            GamePlayer gamePlayer = GameDataManager.Instance.gamePlayerManager.AddGamePlayerId(tcpProtocol, accountId);
            MessageSender.Instance.SendLoginResponse(tcpProtocol, accountId);
        }
        else
        {
            MessageSender.Instance.SendErrorEvent(tcpProtocol, (int)ErrorCodeEnum.AccountDoNotExist);
        }
    }
}

