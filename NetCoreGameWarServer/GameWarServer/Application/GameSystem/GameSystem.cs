﻿public abstract class GameSystem
{
    public abstract void Awake();
    public abstract void Start();
    public abstract void Update();
    public abstract void Destroy();
}
