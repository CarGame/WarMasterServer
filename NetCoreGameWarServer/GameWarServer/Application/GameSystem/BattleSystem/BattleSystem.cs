﻿using CarNetwork;
using GameProtocol;
using System.Collections.Generic;
using UnityEngine;

public class BattleSystem : GameSystem
{
    public override void Awake()
    {

    }

    public override void Start()
    {
        MessageHandler.Instance.OnEnterGameRequestEvent += HandleEnterGameRequest;
        MessageHandler.Instance.OnSyncTransformRequestEvent += OnHandleSyncTransformRequest;
        MessageHandler.Instance.OnMoveToRequestEvent += OnHandleMoveToRequest;
        MessageHandler.Instance.OnMoveTrackRequestEvent += OnHandleMoveTrackRequest;
        MessageHandler.Instance.OnMoveTowardsRequestEvent += OnHandleMoveTowardsRequest;
        MessageHandler.Instance.OnMoveStopRequestEvent += OnHandleMoveStopRequest;
        MessageHandler.Instance.OnSyncPropertyRequestEvent += OnHandleSyncPropertyRequest;
        MessageHandler.Instance.OnAttackRequestEvent += OnHandleAttackRequest;
        MessageHandler.Instance.OnCastSkillRequestEvent += OnHandleCastSkillRequest;

    }

    public override void Update()
    {

    }

    public override void Destroy()
    {
        MessageHandler.Instance.OnEnterGameRequestEvent -= HandleEnterGameRequest;
        MessageHandler.Instance.OnSyncTransformRequestEvent -= OnHandleSyncTransformRequest;
        MessageHandler.Instance.OnMoveToRequestEvent -= OnHandleMoveToRequest;
        MessageHandler.Instance.OnMoveTrackRequestEvent -= OnHandleMoveTrackRequest;
        MessageHandler.Instance.OnMoveTowardsRequestEvent -= OnHandleMoveTowardsRequest;
        MessageHandler.Instance.OnMoveStopRequestEvent -= OnHandleMoveStopRequest;
        MessageHandler.Instance.OnSyncPropertyRequestEvent -= OnHandleSyncPropertyRequest;
        MessageHandler.Instance.OnAttackRequestEvent -= OnHandleAttackRequest;
        MessageHandler.Instance.OnCastSkillRequestEvent -= OnHandleCastSkillRequest;
    }

    public void HandleEnterGameRequest(CarTcpProtocol tcpProtocol, EnterGameRequest EnterGameRequest)
    {
        GamePlayer gamePlayer = GameDataManager.Instance.gamePlayerManager.GetGamePlayerByIp(tcpProtocol);
        if (gamePlayer != null)
        {
            ActorInfoMessage actorInfoMessage = new ActorInfoMessage();
            actorInfoMessage.actorId = gamePlayer.accountId;
            actorInfoMessage.roleId = 0;
            actorInfoMessage.actorType = ActorType.localActor;
            actorInfoMessage.campId = 0;
            actorInfoMessage.profession = 0;
            actorInfoMessage.name = string.Empty;
            actorInfoMessage.level = 0;
            actorInfoMessage.pbTransform = new PBTransform();
            actorInfoMessage.pbTransform.position = new PBVector3();
            actorInfoMessage.pbTransform.position.x = gamePlayer.actor.Position.x;
            actorInfoMessage.pbTransform.position.y = gamePlayer.actor.Position.y;
            actorInfoMessage.pbTransform.position.z = gamePlayer.actor.Position.z;
            actorInfoMessage.pbTransform.eulerAngles = new PBVector3();
            actorInfoMessage.pbTransform.eulerAngles.x = gamePlayer.actor.EulerAngles.x;
            actorInfoMessage.pbTransform.eulerAngles.y = gamePlayer.actor.EulerAngles.y;
            actorInfoMessage.pbTransform.eulerAngles.z = gamePlayer.actor.EulerAngles.z;
            actorInfoMessage.pbTransform.localScale = new PBVector3();
            actorInfoMessage.pbTransform.localScale.x = 1.0f;
            actorInfoMessage.pbTransform.localScale.y = 1.0f;
            actorInfoMessage.pbTransform.localScale.z = 1.0f;

            MessageSender.Instance.SendEnterGameResponse(tcpProtocol, actorInfoMessage);
        }

        BroadcastTransform(gamePlayer);

        List<ActorInfoMessage> actorInfoMessageList = new List<ActorInfoMessage>();
        List<GamePlayer> gamePlayerList = GameDataManager.Instance.gamePlayerManager.GetGamePlayerList();
        if (gamePlayerList != null)
        {
            int gamePlayerCount = gamePlayerList.Count;
            for (int i = 0; i < gamePlayerCount; ++i)
            {
                if (gamePlayerList[i] != null && gamePlayerList[i].accountId != gamePlayer.accountId)
                {
                    ActorInfoMessage actorInfoMessage = new ActorInfoMessage();
                    actorInfoMessage.actorId = gamePlayerList[i].accountId;
                    actorInfoMessage.roleId = 0;
                    actorInfoMessage.actorType = ActorType.networkActor;
                    actorInfoMessage.campId = 0;
                    actorInfoMessage.profession = 0;
                    actorInfoMessage.name = string.Empty;
                    actorInfoMessage.level = 0;
                    actorInfoMessage.pbTransform = new PBTransform();
                    actorInfoMessage.pbTransform.position = new PBVector3();
                    actorInfoMessage.pbTransform.position.x = gamePlayerList[i].actor.Position.x;
                    actorInfoMessage.pbTransform.position.y = gamePlayerList[i].actor.Position.y;
                    actorInfoMessage.pbTransform.position.z = gamePlayerList[i].actor.Position.z;

                    actorInfoMessage.pbTransform.eulerAngles = new PBVector3();
                    actorInfoMessage.pbTransform.eulerAngles.x = gamePlayerList[i].actor.EulerAngles.x;
                    actorInfoMessage.pbTransform.eulerAngles.y = gamePlayerList[i].actor.EulerAngles.y;
                    actorInfoMessage.pbTransform.eulerAngles.z = gamePlayerList[i].actor.EulerAngles.z;

                    actorInfoMessage.pbTransform.localScale = new PBVector3();
                    actorInfoMessage.pbTransform.localScale.x = gamePlayerList[i].actor.LocalScale.x;
                    actorInfoMessage.pbTransform.localScale.y = gamePlayerList[i].actor.LocalScale.y;
                    actorInfoMessage.pbTransform.localScale.z = gamePlayerList[i].actor.LocalScale.z;

                    actorInfoMessageList.Add(actorInfoMessage);
                }
            }

            MessageSender.Instance.SendCreateActorListEvent(gamePlayer.tcpProtocol, actorInfoMessageList);
        }
    }

    private void OnHandleSyncTransformRequest(CarTcpProtocol protocol, SyncTransformRequest syncTransformRequest)
    {
        if (protocol != null && syncTransformRequest != null && syncTransformRequest.pbTransform != null)
        {
            GamePlayer gamePlayer = GameDataManager.Instance.gamePlayerManager.GetGamePlayerByIp(protocol);
            List<GamePlayer> gamePlayerList = GameDataManager.Instance.gamePlayerManager.GetGamePlayerList();
            if (gamePlayer != null && gamePlayerList != null)
            {
                gamePlayer.actor.Position = new Vector3(syncTransformRequest.pbTransform.position.x, syncTransformRequest.pbTransform.position.y, syncTransformRequest.pbTransform.position.z);
                gamePlayer.actor.EulerAngles = new Vector3(syncTransformRequest.pbTransform.eulerAngles.x, syncTransformRequest.pbTransform.eulerAngles.y, syncTransformRequest.pbTransform.eulerAngles.z);
                gamePlayer.actor.LocalScale = new Vector3(syncTransformRequest.pbTransform.localScale.x, syncTransformRequest.pbTransform.localScale.y, syncTransformRequest.pbTransform.localScale.z);
            }
        }
    }

    private void OnHandleMoveToRequest(CarTcpProtocol protocol, MoveToRequest moveToRequest)
    {
        if (protocol != null && moveToRequest != null && moveToRequest.pbTargetPosition != null)
        {
            GamePlayer gamePlayer = GameDataManager.Instance.gamePlayerManager.GetGamePlayerByIp(protocol);
            List<GamePlayer> gamePlayerList = GameDataManager.Instance.gamePlayerManager.GetGamePlayerList();
            if (gamePlayer != null && gamePlayerList != null)
            {
                Vector3 moveToPosition = new Vector3(moveToRequest.pbTargetPosition.x, moveToRequest.pbTargetPosition.y, moveToRequest.pbTargetPosition.z);

                int gamePlayerCount = gamePlayerList.Count;
                for (int i = 0; i < gamePlayerCount; ++i)
                {
                    if (gamePlayerList[i] != null && gamePlayerList[i].tcpProtocol != null)
                    {
                        MessageSender.Instance.SendMoveToResponse(gamePlayerList[i].tcpProtocol, gamePlayer.accountId, moveToPosition);
                    }
                }
            }
        }
    }

    private void OnHandleMoveTrackRequest(CarTcpProtocol protocol, MoveTrackRequest moveTrackRequest)
    {
        if (protocol != null && moveTrackRequest != null && moveTrackRequest.pbTrackPositionList != null && moveTrackRequest.pbTrackPositionList.Count > 0)
        {
            GamePlayer gamePlayer = GameDataManager.Instance.gamePlayerManager.GetGamePlayerByIp(protocol);
            List<GamePlayer> gamePlayerList = GameDataManager.Instance.gamePlayerManager.GetGamePlayerList();
            if (gamePlayer != null && gamePlayerList != null)
            {
                int gamePlayerCount = gamePlayerList.Count;
                for (int i = 0; i < gamePlayerCount; ++i)
                {
                    if (gamePlayerList[i] != null && gamePlayerList[i].tcpProtocol != null)
                    {
                        MessageSender.Instance.SendMoveTrackResponse(gamePlayerList[i].tcpProtocol, gamePlayer.accountId, moveTrackRequest.pbTrackPositionList);
                    }
                }
            }
        }
    }

    private void OnHandleMoveTowardsRequest(CarTcpProtocol protocol, MoveTowardsRequest moveTowardsRequest)
    {
        if (protocol != null && moveTowardsRequest != null && moveTowardsRequest.pbMoveDirection != null && moveTowardsRequest.pbSourcePosition != null)
        {
            GamePlayer gamePlayer = GameDataManager.Instance.gamePlayerManager.GetGamePlayerByIp(protocol);
            List<GamePlayer> gamePlayerList = GameDataManager.Instance.gamePlayerManager.GetGamePlayerList();
            if (gamePlayer != null && gamePlayerList != null)
            {
                gamePlayer.actor.Position = new Vector3(moveTowardsRequest.pbSourcePosition.x, moveTowardsRequest.pbSourcePosition.y, moveTowardsRequest.pbSourcePosition.z);

                int gamePlayerCount = gamePlayerList.Count;
                for (int i = 0; i < gamePlayerCount; ++i)
                {
                    if (gamePlayerList[i] != null && gamePlayerList[i].tcpProtocol != null)
                    {
                        MessageSender.Instance.SendMoveTowardsResponse(gamePlayerList[i].tcpProtocol, gamePlayer.accountId, moveTowardsRequest.pbSourcePosition, moveTowardsRequest.pbMoveDirection);
                    }
                }
            }
        }
    }

    private void OnHandleMoveStopRequest(CarTcpProtocol protocol, MoveStopRequest moveStopRequest)
    {
        if (protocol != null && moveStopRequest != null && moveStopRequest.pbTransform.position != null && moveStopRequest.pbTransform.eulerAngles != null)
        {
            GamePlayer gamePlayer = GameDataManager.Instance.gamePlayerManager.GetGamePlayerByIp(protocol);
            List<GamePlayer> gamePlayerList = GameDataManager.Instance.gamePlayerManager.GetGamePlayerList();
            if (gamePlayer != null && gamePlayerList != null)
            {
                gamePlayer.actor.Position = new Vector3(moveStopRequest.pbTransform.position.x, moveStopRequest.pbTransform.position.y, moveStopRequest.pbTransform.position.z);
                gamePlayer.actor.EulerAngles = new Vector3(moveStopRequest.pbTransform.eulerAngles.x, moveStopRequest.pbTransform.eulerAngles.y, moveStopRequest.pbTransform.eulerAngles.z);
                gamePlayer.actor.LocalScale = new Vector3(moveStopRequest.pbTransform.localScale.x, moveStopRequest.pbTransform.localScale.y, moveStopRequest.pbTransform.localScale.z);

                int gamePlayerCount = gamePlayerList.Count;
                for (int i = 0; i < gamePlayerCount; ++i)
                {
                    if (gamePlayerList[i] != null && gamePlayerList[i].tcpProtocol != null)
                    {
                        MessageSender.Instance.SendMoveStopResponse(gamePlayerList[i].tcpProtocol, gamePlayer.accountId, gamePlayer.actor.Position, gamePlayer.actor.EulerAngles, gamePlayer.actor.LocalScale);
                    }
                }
            }
        }
    }

    private void OnHandleSyncPropertyRequest(CarTcpProtocol protocol, SyncPropertyRequest syncPropertyRequest)
    {
        if (protocol != null && syncPropertyRequest != null)
        {
            GamePlayer gamePlayer = GameDataManager.Instance.gamePlayerManager.GetGamePlayerByIp(protocol);
            List<GamePlayer> gamePlayerList = GameDataManager.Instance.gamePlayerManager.GetGamePlayerList();
            if (gamePlayer != null && gamePlayerList != null)
            {
                List<PropertyItem> propertyItemList = gamePlayer.actor.GetPropertyItemList();

                int gamePlayerCount = gamePlayerList.Count;
                for (int i = 0; i < gamePlayerCount; ++i)
                {
                    if (gamePlayerList[i] != null)
                    {
                        MessageSender.Instance.SendSyncPropertyResponse(gamePlayerList[i].tcpProtocol, gamePlayer.accountId, propertyItemList);
                    }
                }
            }
        }
    }

    private void OnHandleAttackRequest(CarTcpProtocol protocol, AttackRequest attackRequest)
    {
        if (protocol != null && attackRequest != null)
        {
            GamePlayer gamePlayer = GameDataManager.Instance.gamePlayerManager.GetGamePlayerByIp(protocol);
            List<GamePlayer> gamePlayerList = GameDataManager.Instance.gamePlayerManager.GetGamePlayerList();
            if (gamePlayer != null && gamePlayerList != null)
            {
                int gamePlayerCount = gamePlayerList.Count;
                for (int i = 0; i < gamePlayerCount; ++i)
                {
                    if (gamePlayerList[i] != null && gamePlayerList[i].tcpProtocol != null)
                    {
                        MessageSender.Instance.SendAttackResponse(gamePlayerList[i].tcpProtocol, gamePlayer.accountId, gamePlayer.actor.Position, gamePlayer.actor.EulerAngles, gamePlayer.actor.LocalScale);
                    }
                }
            }
        }
    }

    private void OnHandleCastSkillRequest(CarTcpProtocol protocol, CastSkillRequest castSkillRequest)
    {
        if (protocol != null && castSkillRequest != null)
        {
            GamePlayer gamePlayer = GameDataManager.Instance.gamePlayerManager.GetGamePlayerByIp(protocol);
            List<GamePlayer> gamePlayerList = GameDataManager.Instance.gamePlayerManager.GetGamePlayerList();
            if (gamePlayer != null && gamePlayerList != null)
            {
                int gamePlayerCount = gamePlayerList.Count;
                for (int i = 0; i < gamePlayerCount; ++i)
                {
                    if (gamePlayerList[i] != null && gamePlayerList[i].tcpProtocol != null)
                    {
                        MessageSender.Instance.SendMoveStopResponse(gamePlayerList[i].tcpProtocol, gamePlayer.accountId, gamePlayer.actor.Position, gamePlayer.actor.EulerAngles, gamePlayer.actor.LocalScale);
                    }
                }
            }
        }
    }

    private void BroadcastTransform(GamePlayer gamePlayer)
    {
        List<GamePlayer> gamePlayerList = GameDataManager.Instance.gamePlayerManager.GetGamePlayerList();
        if (gamePlayerList != null)
        {
            int gamePlayerCount = gamePlayerList.Count;
            for (int i = 0; i < gamePlayerCount; ++i)
            {
                if (gamePlayerList[i] != null && gamePlayerList[i].tcpProtocol != null && gamePlayerList[i] != gamePlayer)
                {
                    ActorInfoMessage actorInfoMessage = new ActorInfoMessage();
                    actorInfoMessage.actorId = gamePlayer.accountId;
                    actorInfoMessage.roleId = 0;
                    actorInfoMessage.actorType = ActorType.networkActor;
                    actorInfoMessage.campId = 0;
                    actorInfoMessage.profession = 0;
                    actorInfoMessage.name = string.Empty;
                    actorInfoMessage.level = 0;
                    actorInfoMessage.pbTransform = new PBTransform();
                    actorInfoMessage.pbTransform.position = new PBVector3();
                    actorInfoMessage.pbTransform.position.x = gamePlayer.actor.Position.x;
                    actorInfoMessage.pbTransform.position.y = gamePlayer.actor.Position.y;
                    actorInfoMessage.pbTransform.position.z = gamePlayer.actor.Position.z;
                    actorInfoMessage.pbTransform.eulerAngles = new PBVector3();
                    actorInfoMessage.pbTransform.eulerAngles.x = gamePlayer.actor.EulerAngles.x;
                    actorInfoMessage.pbTransform.eulerAngles.y = gamePlayer.actor.EulerAngles.y;
                    actorInfoMessage.pbTransform.eulerAngles.z = gamePlayer.actor.EulerAngles.z;
                    actorInfoMessage.pbTransform.localScale = new PBVector3();
                    actorInfoMessage.pbTransform.localScale.x = 1.0f;
                    actorInfoMessage.pbTransform.localScale.y = 1.0f;
                    actorInfoMessage.pbTransform.localScale.z = 1.0f;

                    MessageSender.Instance.SendCreateActorEvent(gamePlayerList[i].tcpProtocol, actorInfoMessage);
                }
            }
        }
    }
}

