﻿public class ModelTable  
{
    public enum ModelHeader
    {
        Id,
        PathFilename
    }

    public class ModelEntry
    {
        public int id;
        public string pathFilename;
    }

    public const string filename = "Model";

    public static ModelEntry GetEntry(int id)
    {
        ModelEntry modelEntry = new ModelEntry();
        modelEntry.id = id;
        modelEntry.pathFilename = GameDocument.Instance.GetCellContent(filename, id, (int)ModelHeader.PathFilename);

        return modelEntry;
    }
}
