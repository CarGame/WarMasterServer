﻿public class LoadingTable
{
    public enum LoadingHeader
    {
        Id,
        Text,
    }

    public class LoadingEntry
    {
        public int id;
        public string text;
    }

    public const string filename = "Loading";

    public static LoadingEntry GetEntry(int id)
    {
        LoadingEntry loadingEntry = new LoadingEntry();
        loadingEntry.id = id;
        loadingEntry.text = GameDocument.Instance.GetCellContent(filename, id, (int)LoadingHeader.Text);

        return loadingEntry;
    }
}
