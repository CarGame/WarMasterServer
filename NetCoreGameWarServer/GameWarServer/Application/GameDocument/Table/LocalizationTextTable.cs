﻿public class LocalizationTextTable
{
    public enum LocalizationTextHeader
    {
        Id,
        Text,
    }

    public class LocalizationTextEntry
    {
        public int id;
        public string text;
    }

    public const string filename= "LocalizationText";

    public static LocalizationTextEntry GetEntry(int id)
    {
        LocalizationTextEntry localizationTextEntry = new LocalizationTextEntry();
        localizationTextEntry.id = id;
        localizationTextEntry.text = GameDocument.Instance.GetCellContent(filename, id, (int)LocalizationTextHeader.Text);

        return localizationTextEntry;
    }
}
