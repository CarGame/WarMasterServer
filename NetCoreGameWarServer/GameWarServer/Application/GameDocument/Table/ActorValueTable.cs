﻿public class ActorValueTable
{
    public enum ActorValueHeader
    {
        Id = 0,
        BaseLife = 1,
        LifeGrowth = 2,
        LifeRecovery = 3,
        BaseMagic = 4,
        MagicGrowth = 5,
        MagicRecovery = 6,
        BasePhysicsAttack = 7,
        PhysicsAttackGrowth = 8,
        BasePhysicsDefense = 9,
        PhysicsDefenseGrowth = 10,
        BaseMagicAttack = 11,
        MagicAttackGrowth = 12,
        BaseMagicDefense = 13,
        MagicDefenseGrowth = 14,
        BaseMoveSpeed = 15,
        MoveSpeedGrowth = 16,
        BaseAttackSpeed = 17,
        AttackSpeedGrowth = 18,
        BaseAttackDistance = 19,
        AttackDistanceGrowth = 20,
        BasePhysicsCritRate = 21,
        PhysicsCritRateGrowth = 22,
        BaseMagicCritRate = 23,
        MagicCritRateGrowth = 24,
        BaseCritDamageRate = 25,
        CritDamageRateGrowth = 26,
        BasePhysicsDrainRate = 27,
        PhysicsDrainRateGrowth = 28,
        BaseMagicDrainRate = 29,
        MagicDrainRateGrowth = 30,
        BaseCoolDownReduceRate = 31,
        CoolDownReduceRateGrowth = 32,
    }

    public class ActorValueEntry
    {
        public int id;
        public int baseLife;
        public int lifeGrowth;
        public int lifeRecovery;
        public int baseMagic;
        public int magicGrowth;
        public int magicRecovery;
        public int basePhysicsAttack;
        public int physicsAttackGrowth;
        public int basePhysicsDefense;
        public int physicsDefenseGrowth;
        public int baseMagicAttack;
        public int magicAttackGrowth;
        public int baseMagicDefense;
        public int magicDefenseGrowth;
        public float baseMoveSpeed;
        public float moveSpeedGrowth;
        public float baseAttackSpeed;
        public float attackSpeedGrowth;
        public float baseAttackDistance;
        public float attackDistanceGrowth;
        public float basePhysicsCritRate;
        public float physicsCritRateGrowth;
        public float baseMagicCritRate;
        public float magicCritRateGrowth;
        public float baseCritDamageRate;
        public float critDamageRateGrowth;
        public float basePhysicsDrainRate;
        public float physicsDrainRateGrowth;
        public float baseMagicDrainRate;
        public float magicDrainRateGrowth;
        public float baseCoolDownReduceRate;
        public float coolDownReduceRateGrowth;
    }

    public const string filename = "ActorValue";

    public static ActorValueEntry GetEntry(int id)
    {
        ActorValueEntry actorValueEntry = new ActorValueEntry();
        actorValueEntry.id = id;

        actorValueEntry.baseLife = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorValueHeader.BaseLife));
        actorValueEntry.lifeGrowth = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorValueHeader.LifeGrowth));
        actorValueEntry.lifeRecovery = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorValueHeader.LifeRecovery));

        actorValueEntry.baseMagic = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorValueHeader.BaseMagic));
        actorValueEntry.magicGrowth = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorValueHeader.MagicGrowth));
        actorValueEntry.magicRecovery = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorValueHeader.MagicRecovery));

        actorValueEntry.basePhysicsAttack = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorValueHeader.BasePhysicsAttack));
        actorValueEntry.physicsAttackGrowth = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorValueHeader.PhysicsAttackGrowth));

        actorValueEntry.basePhysicsDefense = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorValueHeader.BasePhysicsDefense));
        actorValueEntry.physicsDefenseGrowth = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorValueHeader.PhysicsDefenseGrowth));

        actorValueEntry.baseMagicAttack = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorValueHeader.BaseMagicAttack));
        actorValueEntry.magicAttackGrowth = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorValueHeader.MagicAttackGrowth));

        actorValueEntry.baseMagicDefense = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorValueHeader.BaseMagicDefense));
        actorValueEntry.magicDefenseGrowth = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorValueHeader.MagicDefenseGrowth));

        actorValueEntry.baseMoveSpeed = float.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorValueHeader.BaseMoveSpeed));
        actorValueEntry.moveSpeedGrowth = float.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorValueHeader.MoveSpeedGrowth));

        actorValueEntry.basePhysicsAttack = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorValueHeader.BasePhysicsAttack));
        actorValueEntry.physicsAttackGrowth = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorValueHeader.PhysicsAttackGrowth));

        actorValueEntry.basePhysicsDefense = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorValueHeader.BasePhysicsDefense));
        actorValueEntry.physicsDefenseGrowth = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorValueHeader.PhysicsDefenseGrowth));

        actorValueEntry.baseMagicAttack = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorValueHeader.BaseMagicAttack));
        actorValueEntry.magicAttackGrowth = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorValueHeader.MagicAttackGrowth));

        actorValueEntry.baseMagicDefense = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorValueHeader.BaseMagicDefense));
        actorValueEntry.magicDefenseGrowth = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorValueHeader.MagicDefenseGrowth));

        actorValueEntry.baseMagicDefense = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorValueHeader.BaseMagicDefense));
        actorValueEntry.magicDefenseGrowth = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorValueHeader.MagicDefenseGrowth));

        actorValueEntry.baseMoveSpeed = float.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorValueHeader.BaseMoveSpeed));
        actorValueEntry.moveSpeedGrowth = float.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorValueHeader.MoveSpeedGrowth));

        actorValueEntry.baseAttackSpeed = float.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorValueHeader.BaseAttackSpeed));
        actorValueEntry.attackSpeedGrowth = float.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorValueHeader.AttackSpeedGrowth));

        actorValueEntry.baseAttackDistance = float.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorValueHeader.BaseAttackDistance));
        actorValueEntry.attackDistanceGrowth = float.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorValueHeader.AttackDistanceGrowth));

        actorValueEntry.basePhysicsCritRate = float.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorValueHeader.BasePhysicsCritRate));
        actorValueEntry.physicsCritRateGrowth = float.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorValueHeader.PhysicsCritRateGrowth));

        actorValueEntry.baseMagicCritRate = float.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorValueHeader.BaseMagicCritRate));
        actorValueEntry.magicCritRateGrowth = float.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorValueHeader.MagicCritRateGrowth));

        actorValueEntry.baseCritDamageRate = float.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorValueHeader.BaseCritDamageRate));
        actorValueEntry.critDamageRateGrowth = float.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorValueHeader.CritDamageRateGrowth));

        actorValueEntry.basePhysicsDrainRate = float.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorValueHeader.BasePhysicsDrainRate));
        actorValueEntry.physicsDrainRateGrowth = float.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorValueHeader.PhysicsDrainRateGrowth));

        actorValueEntry.baseMagicDrainRate = float.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorValueHeader.BaseMagicDrainRate));
        actorValueEntry.magicDrainRateGrowth = float.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorValueHeader.MagicDrainRateGrowth));

        actorValueEntry.baseCoolDownReduceRate = float.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorValueHeader.BaseCoolDownReduceRate));
        actorValueEntry.coolDownReduceRateGrowth = float.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)ActorValueHeader.CoolDownReduceRateGrowth));

        return actorValueEntry;
    }
}
