﻿public class RoleValueTable
{
    public enum RoleValueHeader
    {
        Id,
        Level,
        MaxExperience,
    }

    public class RoleValueEntry
    {
        public int id;
        public int level;
        public int maxExperience;
    }

    public const string filename = "RoleValue";

    public static RoleValueEntry GetEntry(int id)
    {
        RoleValueEntry roleValueEntry = new RoleValueEntry();
        roleValueEntry.id = id;
        roleValueEntry.level = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)RoleValueHeader.Level));
        roleValueEntry.maxExperience = int.Parse(GameDocument.Instance.GetCellContent(filename, id, (int)RoleValueHeader.MaxExperience));

        return roleValueEntry;
    }
}
