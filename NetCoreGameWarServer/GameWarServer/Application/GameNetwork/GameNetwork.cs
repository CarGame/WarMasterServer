﻿using System;
using System.IO;
using GameProtocol;

namespace CarNetwork
{
    public class GameNetwork : Singleton<GameNetwork>
    {
        public event Action<string> OnErrorEvent;
        public event CarTcpServer.OnTcpEndPointAction OnEndPointConnectedEvent;
        public event CarTcpServer.OnTcpEndPointAction OnEndPointDisconnectEvent;
        public event CarTcpServer.OnTcpEndPointAction OnEndPointTimeoutEvent;

        private CarTcpServer mTcpServer = new CarTcpServer();

        public bool IsActive
        {
            get
            {
                return mTcpServer.IsActive;
            }
        }

        public bool IsListening
        {
            get
            {
                return mTcpServer.IsListening;
            }
        }

        public int TcpPort
        {
            get
            {
                return mTcpServer.TcpPort;
            }
        }

        public int PlayerCount
        {
            get
            {
                return mTcpServer.PlayerCount;
            }
        }

        public int TimeoutTime
        {
            get
            {
                return mTcpServer.TimeoutTime;
            }
            set
            {
                mTcpServer.TimeoutTime = value;
            }
        }

        public void Awake()
        {
            mTcpServer.OnErrorEvent += OnError;
            mTcpServer.OnEndPointConnectedEvent += OnEndPointConnected;
            mTcpServer.OnEndPointDisconnectEvent += OnEndPointDisconnect;
            mTcpServer.OnEndPointTimeoutEvent += OnEndPointTimeout;
        }

        public bool Start(int tcpPort)
        {
            if (tcpPort > 0)
            {
                if (!mTcpServer.Start(tcpPort))
                {
                    mTcpServer.Stop();
                    Console.WriteLine("server start failed");
                }
                else
                {
                    Console.WriteLine("Game server started on port " + tcpPort);
                    MessageHandler.Instance.Start();
                    MessageSender.Instance.Start();
                }
            }

            return false;
        }

        public void Stop()
        {
            mTcpServer.Stop();
        }

        public void Destroy()
        {
            MessageHandler.Instance.Destroy();
            MessageSender.Instance.Destroy();

            mTcpServer.OnErrorEvent -= OnError;
            mTcpServer.OnEndPointConnectedEvent -= OnEndPointConnected;
            mTcpServer.OnEndPointDisconnectEvent -= OnEndPointDisconnect;
            mTcpServer.OnEndPointTimeoutEvent -= OnEndPointTimeout;
        }

        public void RegisterHandler(int packet, CarTcpServer.OnCustomPacket onCustomPacket)
        {
            if (mTcpServer != null)
            {
                mTcpServer.RegisterHandler(packet, onCustomPacket);
            }
        }

        public void UnregisterHandler(int packet)
        {
            if (mTcpServer != null)
            {
                mTcpServer.UnregisterHandler(packet);
            }
        }

        public void OnError(string message)
        {
            if (OnErrorEvent != null)
            {
                OnErrorEvent(message);
            }
        }

        public void OnEndPointConnected(CarTcpProtocol protocol)
        {
            if (OnEndPointConnectedEvent != null)
            {
                OnEndPointConnectedEvent(protocol);
            }
        }

        public void OnEndPointDisconnect(CarTcpProtocol protocol)
        {
            if (OnEndPointDisconnectEvent != null)
            {
                OnEndPointDisconnectEvent(protocol);
            }
        }

        public void OnEndPointTimeout(CarTcpProtocol protocol)
        {
            if (OnEndPointTimeoutEvent != null)
            {
                OnEndPointTimeoutEvent(protocol);
            }
        }
    }
}

