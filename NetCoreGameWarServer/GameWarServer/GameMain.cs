﻿using System;

public class GameMain
{
    static void Main(string[] args)
    {
        try
        {
            GameApplication gameApplication = new GameApplication();
            gameApplication.Awake();
            gameApplication.Start();

            while (!gameApplication.ShouldQuit())
            {
                gameApplication.Update();
            }
            gameApplication.Destroy();
        }
        catch (Exception exception)
        {
            Console.WriteLine(exception.Message);
        }

        Console.Read();
    }
}
