﻿using System.IO;
using System.Net;
using System.Collections.Generic;
using System;

namespace CarNetwork
{
    public class CarTcpClient
    {
        public event Action<string> OnErrorEvent;
        public event Action OnConnectedEvent;
        public event Action OnServerDisconnectedEvent;
        public event Action OnConnectionTimeoutEvent;

        public delegate void OnCustomPacket(CarBuffer buffer, BinaryReader reader, int packet);

        private Dictionary<int, OnCustomPacket> mMessageHandler = new Dictionary<int, OnCustomPacket>();
        private CarTcpProtocol mTcpProtocol = new CarTcpProtocol();
        private CarBuffer mBuffer;

        public bool IsConnected
        {
            get
            {
                return mTcpProtocol.IsConnected;
            }
        }

        public bool IsTryingToConnect
        {
            get
            {
                return mTcpProtocol.IsTryingToConnect;
            }
        }

        public string Address
        {
            get
            {
                if (mTcpProtocol != null)
                {
                    return mTcpProtocol.Address;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public void Awake()
        {
            mTcpProtocol.OnErrorEvent += OnError;
            mTcpProtocol.OnConnectedEvent += OnConnected;
            mTcpProtocol.OnServerDisconnectedEvent += OnServerDisconnected;
            mTcpProtocol.OnConnectionTimeoutEvent += OnConnectionTimeout;
        }

        public void Start()
        {

        }

        public void Destroy()
        {
            mTcpProtocol.OnErrorEvent -= OnError;
            mTcpProtocol.OnConnectedEvent -= OnConnected;
            mTcpProtocol.OnServerDisconnectedEvent -= OnServerDisconnected;
            mTcpProtocol.OnConnectionTimeoutEvent -= OnConnectionTimeout;

            if (IsConnected)
            {
                Disconnect();
            }
        }

        public void Update()
        {
            mTcpProtocol.DoUpdate();

            ProcessPackets();
        }

        public BinaryWriter BeginSend(int packet)
        {
            mBuffer = CarBuffer.Create();
            return mBuffer.BeginPacket(packet);
        }

        public void EndSend()
        {
            if (mBuffer != null)
            {
                mBuffer.EndPacket();
                mTcpProtocol.SendTcpPacket(mBuffer);
                mBuffer.Recycle();
                mBuffer = null;
            }
        }

        public void Connect(string serverIP, string serverPort)
        {
            int serverPortNumber = int.Parse(serverPort);
            IPEndPoint serverIPEndPoint = CarNetworkTools.ResolveEndPoint(serverIP, serverPortNumber);
            Connect(serverIPEndPoint);
        }

        public void Connect(IPEndPoint ipEndPoint)
        {
            Disconnect();
            mTcpProtocol.Connect(ipEndPoint);
        }

        public void Disconnect()
        {
            mTcpProtocol.Disconnect();
        }

        private void ProcessPackets()
        {
            CarBuffer buffer = null;
            bool keepGoing = true;

            while (keepGoing && mTcpProtocol.ReceivePacket(out buffer))
            {
                keepGoing = ProcessPacket(buffer);
                buffer.Recycle();
            }
        }

        private bool ProcessPacket(CarBuffer buffer)
        {
            BinaryReader reader = buffer.Read();
            if (buffer.Size == 0)
            {
                return true;
            }

            int packet = reader.ReadInt32();

            if (mMessageHandler.ContainsKey(packet))
            {
                mMessageHandler[packet](buffer, reader, packet);
                return true;
            }

            return false;
        }

        public void RegisterHandler(int packet, OnCustomPacket onCustomPacket)
        {
            if (mMessageHandler != null)
            {
                mMessageHandler.Add(packet, onCustomPacket);
            }
        }

        public void UnregisterHandler(int packet)
        {
            if (mMessageHandler != null && mMessageHandler.ContainsKey(packet))
            {
                mMessageHandler.Remove(packet);
            }
        }

        private void OnError(string message)
        {
            if (OnErrorEvent != null)
            {
                OnErrorEvent(message);
            }
        }

        private void OnConnected()
        {
            if (OnConnectedEvent != null)
            {
                OnConnectedEvent();
            }
        }

        private void OnServerDisconnected()
        {
            if (OnServerDisconnectedEvent != null)
            {
                OnServerDisconnectedEvent();
            }
        }

        private void OnConnectionTimeout()
        {
            if (OnConnectionTimeoutEvent != null)
            {
                OnConnectionTimeoutEvent();
            }
        }
    }
}