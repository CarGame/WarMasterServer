﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Net;
using System.Threading;

namespace CarNetwork
{
    public class CarTcpProtocol
    {
        public enum Stage
        {
            NotConnected,
            Connecting,
            Connected,
        }

        private static CarBuffer mBuffer;

        public Stage currentStage = Stage.NotConnected;
        public IPEndPoint tcpEndPoint;
        public long lastReceivedTime;

        public event Action<string> OnErrorEvent;
        public event Action OnConnectedEvent;
        public event Action OnServerDisconnectedEvent;
        public event Action OnConnectionTimeoutEvent;

        private Queue<CarBuffer> mInputQueue = new Queue<CarBuffer>();
        private Queue<CarBuffer> mOutputQueue = new Queue<CarBuffer>();
        private byte[] tempCache = new byte[8192];
        private CarBuffer mReceivedBuffer;
        private int mExpected;
        private int mOffset;
        private Socket mSocket;
        private BetterList<Socket> mConnectingList = new BetterList<Socket>();

        private const int ticksPerMillisecond = 10000;
        private const int ConnectionTimeoutTime = 15000;

        private bool mConnectedTrigger;
        private bool mServerDisconnectedTrigger;
        private bool mConnectionTimeoutTrigger;

        public bool IsConnected
        {
            get
            {
                return currentStage == Stage.Connected;
            }
        }

        public bool IsTryingToConnect
        {
            get
            {
                return mConnectingList.size != 0;
            }
        }

        public string Address
        {
            get
            {
                return (tcpEndPoint != null) ? tcpEndPoint.ToString() : "0.0.0.0:0";
            }
        }

        public void Connect(IPEndPoint ipEndPoint)
        {
            if (ipEndPoint != null)
            {
                currentStage = Stage.Connecting;
                tcpEndPoint = ipEndPoint;

                try
                {
                    lock (mConnectingList)
                    {
                        mSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                        mConnectingList.Add(mSocket);
                    }

                    IAsyncResult result = mSocket.BeginConnect(tcpEndPoint, OnConnectResult, mSocket);
                    Thread thread = new Thread(CancelConnect);
                    thread.Start(result);
                }
                catch (Exception exception)
                {
                    Error(exception.Message);
                }
            }
            else
            {
                Error("unable to resolve the specified address");
            }
        }

        private void CancelConnect(object state)
        {
            IAsyncResult result = (IAsyncResult)state;

            if (result != null && !result.AsyncWaitHandle.WaitOne(ConnectionTimeoutTime, true))
            {
                try
                {
                    Socket socket = (Socket)result.AsyncState;

                    if (socket != null)
                    {
                        socket.Close();

                        lock (mConnectingList)
                        {
                            if (mConnectingList.size > 0 && mConnectingList[mConnectingList.size - 1] == socket)
                            {
                                mConnectionTimeoutTrigger = true;
                                Close();
                            }

                            mConnectingList.Remove(socket);
                        }
                    }
                }
                catch (Exception exception)
                {
                    Error(exception.Message);
                }
            }
        }

        private void OnConnectResult(IAsyncResult result)
        {
            Socket socket = (Socket)result.AsyncState;

            if (socket == null)
            {
                return;
            }

            if (mSocket != null && socket == mSocket)
            {
                bool success = true;

                try
                {
                    socket.EndConnect(result);
                }
                catch (Exception exception)
                {
                    if (socket == mSocket)
                    {
                        mSocket = null;
                    }

                    Error(exception.Message);

                    socket.Close();
                    success = false;
                }

                if (success)
                {
                    currentStage = Stage.Connected;
                    mConnectedTrigger = true;

                    StartReceiving();
                }
                else
                {
                    Close();
                }
            }

            lock (mConnectingList)
            {
                mConnectingList.Remove(socket);
            }
        }

        public void Disconnect()
        {
            try
            {
                lock (mConnectingList)
                {
                    for (int i = mConnectingList.size - 1; i >= 0; --i)
                    {
                        Socket socket = mConnectingList[i];
                        mConnectingList.RemoveAt(i);
                        if (socket != null)
                        {
                            socket.Close();
                        }
                    }
                }

                if (mSocket != null)
                {
                    Close();
                }
            }
            catch (Exception exception)
            {
                Error(exception.Message);
                lock (mConnectingList)
                {
                    mConnectingList.Clear();
                }

                mSocket = null;
            }
        }

        public void Close()
        {
            currentStage = Stage.NotConnected;

            if (mReceivedBuffer != null)
            {
                mReceivedBuffer.Recycle();
                mReceivedBuffer = null;
            }

            if (mSocket != null)
            {
                if (mSocket.Connected)
                {
                    mSocket.Shutdown(SocketShutdown.Both);
                }

                mSocket.Close();
                mSocket = null;
            }
        }

        public void Release()
        {
            currentStage = Stage.NotConnected;

            if (mSocket != null)
            {
                try
                {
                    if (mSocket.Connected)
                    {
                        mSocket.Shutdown(SocketShutdown.Both);
                    }

                    mSocket.Close();
                }
                catch (Exception exception)
                {
                    Error(exception.Message);
                }

                mSocket = null;
            }

            CarBuffer.Recycle(mInputQueue);
            CarBuffer.Recycle(mOutputQueue);
        }

        public BinaryWriter BeginSend(int packet)
        {
            mBuffer = CarBuffer.Create(false);
            BinaryWriter binaryWriter = mBuffer.BeginPacket(packet);

            return binaryWriter;
        }

        public void EndSend()
        {
            mBuffer.EndPacket();
            SendTcpPacket(mBuffer);
            mBuffer = null;
        }

        public void SendTcpPacket(CarBuffer buffer)
        {
            buffer.Retain();

            if (mSocket != null && mSocket.Connected)
            {
                buffer.Read();

                lock (mOutputQueue)
                {
                    mOutputQueue.Enqueue(buffer);

                    if (mOutputQueue.Count == 1)
                    {
                        try
                        {
                            mSocket.BeginSend(buffer.StreamBuffer, buffer.Position, buffer.Size, SocketFlags.None, OnSend, buffer);
                        }
                        catch (Exception exception)
                        {
                            Error(exception.Message);
                            Close();
                            Release();
                        }
                    }
                }
            }
            else
            {
                buffer.Recycle();
            }
        }

        private void OnSend(IAsyncResult result)
        {
            if (currentStage == Stage.NotConnected)
            {
                return;
            }

            int bytes = 0;

            try
            {
                bytes = mSocket.EndSend(result);
            }
            catch (System.Exception exception)
            {
                bytes = 0;
                Close();
                Error(exception.Message);

                return;
            }

            lock (mOutputQueue)
            {
                mOutputQueue.Dequeue().Recycle();

                if (bytes > 0)
                {
                    if (mOutputQueue.Count > 0)
                    {
                        CarBuffer carBuffer = mOutputQueue.Peek();
                        if (carBuffer != null)
                        {
                            mSocket.BeginSend(carBuffer.StreamBuffer, carBuffer.Position, carBuffer.Size, SocketFlags.None, OnSend, carBuffer);
                        }
                    }
                }
                else
                {
                    Close();
                }
            }
        }

        public void StartReceiving()
        {
            StartReceiving(null);
        }

        public void StartReceiving(Socket socket)
        {
            if (socket != null)
            {
                Close();
                mSocket = socket;
            }

            if (mSocket != null && mSocket.Connected)
            {
                lastReceivedTime = DateTime.Now.Ticks / ticksPerMillisecond;
                tcpEndPoint = (IPEndPoint)mSocket.RemoteEndPoint;

                try
                {
                    mSocket.BeginReceive(tempCache, 0, tempCache.Length, SocketFlags.None, OnReceive, null);
                }
                catch (Exception exception)
                {
                    Error(exception.Message);
                    Disconnect();
                }
            }
        }

        public bool ReceivePacket(out CarBuffer buffer)
        {
            if (mInputQueue.Count != 0)
            {
                lock (mInputQueue)
                {
                    buffer = mInputQueue.Dequeue();
                    return true;
                }
            }

            buffer = null;
            return false;
        }

        private void OnReceive(IAsyncResult result)
        {
            if (currentStage != Stage.NotConnected)
            {
                try
                {
                    if (mSocket != null)
                    {
                        int bytes = mSocket.EndReceive(result);

                        if (bytes > 0 && ProcessBuffer(bytes))
                        {
                            mSocket.BeginReceive(tempCache, 0, tempCache.Length, SocketFlags.None, OnReceive, null);
                            lastReceivedTime = DateTime.Now.Ticks / ticksPerMillisecond;
                        }
                    }
                }
                catch (Exception exception)
                {
                    Error(exception.Message);
                    mServerDisconnectedTrigger = true;
                    Disconnect();
                }
            }
        }

        private bool ProcessBuffer(int bytes)
        {
            if (mReceivedBuffer == null)
            {
                mReceivedBuffer = CarBuffer.Create();
                BinaryWriter binaryWriter = mReceivedBuffer.BeginWriting(false);
                binaryWriter.Write(tempCache, 0, bytes);
            }
            else
            {
                BinaryWriter binaryWriter = mReceivedBuffer.BeginWriting(true);
                binaryWriter.Write(tempCache, 0, bytes);
            }

            for (int available = mReceivedBuffer.Size - mOffset; available >= 4; )
            {
                if (mExpected == 0)
                {
                    mExpected = mReceivedBuffer.PeekInt(mOffset);
                    if (mExpected == -1)
                    {
                        break;
                    }

                    if (mExpected == 0)
                    {
                        Close();
                        return false;
                    }
                }

                available -= 4;

                if (available == mExpected)
                {
                    mReceivedBuffer.Read(mOffset + 4);

                    lock (mInputQueue)
                    {
                        mInputQueue.Enqueue(mReceivedBuffer);
                    }

                    mReceivedBuffer = null;
                    mExpected = 0;
                    mOffset = 0;
                    break;
                }
                else if (available > mExpected)
                {
                    int realSize = mExpected + 4;
                    CarBuffer buffer = CarBuffer.Create();

                    BinaryWriter binaryWriter = buffer.BeginWriting(false);
                    binaryWriter.Write(mReceivedBuffer.StreamBuffer, mOffset, realSize);
                    buffer.Read(4);

                    lock (mInputQueue)
                    {
                        mInputQueue.Enqueue(buffer);
                    }

                    available -= mExpected;
                    mOffset += realSize;
                    mExpected = 0;
                }
                else
                {
                    break;
                }
            }

            return true;
        }

        public void DoUpdate()
        {
            if (mConnectedTrigger)
            {
                if (OnConnectedEvent != null)
                {
                    OnConnectedEvent();
                }

                mConnectedTrigger = false;
            }

            if (mServerDisconnectedTrigger)
            {
                if (OnServerDisconnectedEvent != null)
                {
                    OnServerDisconnectedEvent();
                }

                mServerDisconnectedTrigger = false;
            }

            if (mConnectionTimeoutTrigger)
            {
                if (OnConnectionTimeoutEvent != null)
                {
                    OnConnectionTimeoutEvent();
                }

                mConnectionTimeoutTrigger = false;
            }
        }

        private void Error(string message)
        {
            if (OnErrorEvent != null)
            {
                OnErrorEvent(message);
            }
        }
    }
}