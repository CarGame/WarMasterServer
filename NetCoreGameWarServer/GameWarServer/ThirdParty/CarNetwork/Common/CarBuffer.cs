﻿using System.IO;
using System.Threading;
using System.Collections.Generic;

namespace CarNetwork
{
    public class CarBuffer
    {
        private static BetterList<CarBuffer> bufferPool = new BetterList<CarBuffer>();

        public static CarBuffer Create()
        {
            return Create(true);
        }

        private MemoryStream mStream;
        private BinaryWriter mWriter;
        private BinaryReader mReader;

        private int mReference;
        private int mSize;
        private bool mWriting;
        private bool mInPool;

        private CarBuffer()
        {
            mStream = new MemoryStream();
            mWriter = new BinaryWriter(mStream);
            mReader = new BinaryReader(mStream);
        }

        ~CarBuffer()
        {
            if (mStream != null)
            {
                mStream.Close();
                mStream.Dispose();
                mStream = null;
            }

            if (mWriter != null)
            {
                mWriter.Close();
                mWriter = null;
            }

            if (mReader != null)
            {
                mReader.Close();
                mReader = null;
            }
        }

        public int Size
        {
            get
            {
                return mWriting ? (int)mStream.Position : mSize - (int)mStream.Position;
            }
        }

        public int Position
        {
            get
            {
                return (int)mStream.Position;
            }

            set
            {
                mStream.Seek(value, SeekOrigin.Begin);
            }
        }

        public byte[] StreamBuffer
        {
            get
            {
                return mStream.GetBuffer();
            }
        }

        public static CarBuffer Create(bool markAsUsed)
        {
            CarBuffer buffer = null;

            if (bufferPool.size == 0)
            {
                buffer = new CarBuffer();
            }
            else
            {
                lock (bufferPool)
                {
                    if (bufferPool.size != 0)
                    {
                        buffer = bufferPool.Pop();
                        buffer.mInPool = false;
                    }
                    else
                    {
                        buffer = new CarBuffer();
                    }
                }
            }
            buffer.mReference = markAsUsed ? 1 : 0;

            return buffer;
        }

        public bool Recycle()
        {
            return Recycle(true);
        }

        public bool Recycle(bool checkUsedFlag)
        {
            if (!mInPool && (!checkUsedFlag || Release()))
            {
                mInPool = true;

                lock (bufferPool)
                {
                    Clear();
                    bufferPool.Add(this);
                }
                return true;
            }

            return false;
        }

        public static void Recycle(Queue<CarBuffer> carBufferQueue)
        {
            lock (bufferPool)
            {
                while (carBufferQueue.Count != 0)
                {
                    CarBuffer buffer = carBufferQueue.Dequeue();
                    buffer.Clear();
                    bufferPool.Add(buffer);
                }
            }
        }

        public static void Recycle(Queue<CarDatagram> datagramQueue)
        {
            lock (bufferPool)
            {
                while (datagramQueue.Count != 0)
                {
                    CarBuffer buffer = datagramQueue.Dequeue().buffer;
                    buffer.Clear();
                    bufferPool.Add(buffer);
                }
            }
        }

        public static void Recycle(BetterList<CarBuffer> bufferList)
        {
            lock (bufferPool)
            {
                for (int i = 0; i < bufferList.size; ++i)
                {
                    CarBuffer buffer = bufferList[i];
                    buffer.Clear();
                    bufferPool.Add(buffer);
                }

                bufferList.Clear();
            }
        }

        public void Retain()
        {
            Interlocked.Increment(ref mReference);
        }

        public bool Release()
        {
            if (Interlocked.Decrement(ref mReference) > 0)
            {
                return false;
            }

            mSize = 0;
            mStream.Seek(0, SeekOrigin.Begin);
            mWriting = true;

            return true;
        }

        public void Clear()
        {
            mReference = 0;
            mSize = 0;
            if (mStream.Capacity > 1024)
            {
                mStream.SetLength(256);
            }
            mStream.Seek(0, SeekOrigin.Begin);
            mWriting = false;
        }

        public BinaryWriter BeginWriting(bool append)
        {
            if (!append || !mWriting)
            {
                mStream.Seek(0, SeekOrigin.Begin);
                mSize = 0;
            }
            mWriting = true;

            return mWriter;
        }

        public BinaryWriter BeginWriting(int startOffset)
        {
            mStream.Seek(startOffset, SeekOrigin.Begin);
            mWriting = true;

            return mWriter;
        }

        public int EndWriting()
        {
            if (mWriting)
            {
                mSize = Position;
                mStream.Seek(0, SeekOrigin.Begin);
                mWriting = false;
            }

            return mSize;
        }

        public BinaryReader Read()
        {
            if (mWriting)
            {
                mWriting = false;
                mSize = (int)mStream.Position;
                mStream.Seek(0, SeekOrigin.Begin);
            }

            return mReader;
        }

        public BinaryReader Read(int startOffset)
        {
            if (mWriting)
            {
                mWriting = false;
                mSize = (int)mStream.Position;
            }
            mStream.Seek(startOffset, SeekOrigin.Begin);

            return mReader;
        }

        public int PeekByte(int offset)
        {
            long position = mStream.Position;
            if (offset + 1 > position)
            {
                return -1;
            }
            mStream.Seek(offset, SeekOrigin.Begin);
            int byteValue = mReader.ReadByte();
            mStream.Seek(position, SeekOrigin.Begin);

            return byteValue;
        }

        public int PeekInt(int offset)
        {
            long position = mStream.Position;
            if (offset + 4 > position)
            {
                return -1;
            }
            mStream.Seek(offset, SeekOrigin.Begin);
            int intValue = mReader.ReadInt32();
            mStream.Seek(position, SeekOrigin.Begin);

            return intValue;
        }

        public BinaryWriter BeginPacket(int packet)
        {
            BinaryWriter writer = BeginWriting(false);
            writer.Write(0);
            writer.Write((int)packet);

            return writer;
        }

        public BinaryWriter BeginPacket(int packet, int startOffset)
        {
            BinaryWriter writer = BeginWriting(startOffset);
            writer.Write(0);
            writer.Write((int)packet);

            return writer;
        }

        public int EndPacket()
        {
            if (mWriting)
            {
                mSize = Position;
                mStream.Seek(0, SeekOrigin.Begin);
                mWriter.Write(mSize - 4);
                mStream.Seek(0, SeekOrigin.Begin);
                mWriting = false;
            }

            return mSize;
        }

        public int EndTcpPacketStartingAt(int startOffset)
        {
            if (mWriting)
            {
                mSize = Position;
                mStream.Seek(startOffset, SeekOrigin.Begin);
                mWriter.Write(mSize - 4 - startOffset);
                mStream.Seek(0, SeekOrigin.Begin);
                mWriting = false;
            }

            return mSize;
        }

        public int EndTcpPacketWithOffset(int offset)
        {
            if (mWriting)
            {
                mSize = Position;
                mStream.Seek(0, SeekOrigin.Begin);
                mWriter.Write(mSize - 4);
                mStream.Seek(offset, SeekOrigin.Begin);
                mWriting = false;
            }

            return mSize;
        }

        public void MarkAsUsed()
        {
            Interlocked.Increment(ref mReference);
        }
    }
}