﻿using ProtoBuf;
using System.IO;

namespace CarNetwork
{
    public class CarMessageConverter : Singleton<CarMessageConverter>
    {
        public T Deserialize<T>(CarBuffer buffer)
        {
            MemoryStream memoryStream = new MemoryStream();
            memoryStream.Write(buffer.StreamBuffer, buffer.Position, buffer.Size);
            memoryStream.Position = 0;
            T t = Serializer.Deserialize<T>(memoryStream);

            return t;
        }

        public MemoryStream Serialize<T>(T t)
        {
            MemoryStream memoryStream = new MemoryStream();
            Serializer.Serialize<T>(memoryStream, t);
            return memoryStream;
        }
    }
}