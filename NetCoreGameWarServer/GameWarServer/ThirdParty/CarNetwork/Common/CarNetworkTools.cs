﻿using System.Net;
using System.Net.Sockets;

namespace CarNetwork
{
    public class CarNetworkTools
    {
        private static IPAddress mLocalAddress;

        //Generate a random port from 10,000 to 60,000.
        static public int randomPort
        {
            get
            {
                return 10000 + (int)(System.DateTime.Now.Ticks % 50000);
            }
        }

        static public IPAddress localAddress
        {
            get
            {
                if (mLocalAddress == null)
                {
                    mLocalAddress = IPAddress.Loopback;

                    IPHostEntry ipHostEntry = Dns.GetHostEntry(Dns.GetHostName());
                    if (ipHostEntry != null && ipHostEntry.AddressList != null)
                    {
                        int ipAddressCount = ipHostEntry.AddressList.Length;
                        for (int i = 0; i < ipAddressCount; ++i)
                        {
                            if (IsValidAddress(ipHostEntry.AddressList[i]))
                            {
                                mLocalAddress = ipHostEntry.AddressList[i];
                                break;
                            }
                        }
                    }
                }

                return mLocalAddress;
            }
        }

        static public bool IsValidAddress(IPAddress address)
        {
            if (address.AddressFamily != AddressFamily.InterNetwork)
            {
                return false;
            }

            if (address.Equals(IPAddress.Loopback))
            {
                return false;
            }

            if (address.Equals(IPAddress.None))
            {
                return false;
            }

            if (address.Equals(IPAddress.Any))
            {
                return false;
            }

            return true;
        }

        static public IPAddress ResolveAddress(string address)
        {
            if (string.IsNullOrEmpty(address))
            {
                return null;
            }

            IPAddress ip = null;
            if (IPAddress.TryParse(address, out ip))
            {
                return ip;
            }

            IPAddress[] ipAddressArray = Dns.GetHostAddresses(address);

            for (int i = 0; i < ipAddressArray.Length; ++i)
            {
                if (!IPAddress.IsLoopback(ipAddressArray[i]))
                {
                    return ipAddressArray[i];
                }
            }

            return null;
        }

        public static IPEndPoint ResolveEndPoint(string address, int port)
        {
            IPEndPoint ip = ResolveEndPoint(address);
            if (ip != null)
            {
                ip.Port = port;
            }

            return ip;
        }

        public static IPEndPoint ResolveEndPoint(string address)
        {
            int port = 0;
            string[] split = address.Split(':');

            if (split.Length > 1)
            {
                address = split[0];
                int.TryParse(split[1], out port);
            }

            IPAddress ipAddress = ResolveAddress(address);
            if (ipAddress != null)
            {
                return new IPEndPoint(ipAddress, port);
            }
            else
            {
                return null;
            }
        }

        // Converts 192.168.1.1 to 192.168.1.
        public static string GetSubnet(IPAddress ip)
        {
            if (ip != null)
            {
                string address = ip.ToString();
                int last = address.LastIndexOf('.');
                if (last != -1)
                {
                    return address.Substring(0, last);
                }
            }

            return string.Empty;
        }
    }
}