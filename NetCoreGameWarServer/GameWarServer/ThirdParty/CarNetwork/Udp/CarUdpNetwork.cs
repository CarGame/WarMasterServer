﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading;

namespace CarNetwork
{
    public class CarUdpNetwork
    {
        public delegate void OnCustomPacket(IPEndPoint ip, CarBuffer buffer, BinaryReader reader, int packet);
        public delegate void OnTcpEndPointAction(CarUdpProtocol protocol);

        public event Action<string> OnErrorEvent;

        private CarUdpProtocol mUdpProtocol;
        private Thread mThread;
        private CarBuffer mBuffer;
        private Dictionary<int, OnCustomPacket> mMessageHandler = new Dictionary<int, OnCustomPacket>();

        public int Port
        {
            get
            {
                if (mUdpProtocol != null)
                {
                    return mUdpProtocol.IsActive ? mUdpProtocol.ListeningPort : 0;
                }

                return 0;
            }
        }

        public bool IsActive
        {
            get
            {
                return (mUdpProtocol != null && mUdpProtocol.IsActive);
            }
        }

        public bool Start(int listenPort)
        {
            mUdpProtocol = new CarUdpProtocol();

            if (!mUdpProtocol.Start(listenPort))
            {
                return false;
            }

            mThread = new Thread(ThreadFunction);
            mThread.Start();

            return true;
        }

        public void Stop()
        {
            if (mThread != null)
            {
                mThread.Abort();
                mThread = null;
            }

            if (mUdpProtocol != null)
            {
                mUdpProtocol.Stop();
                mUdpProtocol = null;
            }
        }

        private void ThreadFunction()
        {
            while (true)
            {
                CarBuffer buffer;
                IPEndPoint ip;

                while (mUdpProtocol != null && mUdpProtocol.ListeningPort != 0 && mUdpProtocol.ReceivePacket(out buffer, out ip))
                {
                    try
                    {
                        ProcessPacket(buffer, ip);
                    }
                    catch (Exception exception)
                    {
                        Error(exception.Message);
                    }

                    if (buffer != null)
                    {
                        buffer.Recycle();
                        buffer = null;
                    }
                }

                Thread.Sleep(1);
            }
        }

        private bool ProcessPacket(CarBuffer buffer, IPEndPoint ip)
        {
            BinaryReader reader = buffer.Read();
            int request = reader.ReadInt32();

            if (mMessageHandler.ContainsKey(request))
            {
                if (mMessageHandler[request] != null)
                {
                    mMessageHandler[request](ip, buffer, reader, request);

                    return true;
                }
            }

            return false;
        }

        public BinaryWriter BeginSend(int packet)
        {
            mBuffer = CarBuffer.Create();
            BinaryWriter writer = mBuffer.BeginPacket(packet);

            return writer;
        }

        public void EndSend(IPEndPoint ip)
        {
            mBuffer.EndPacket();
            mUdpProtocol.Send(mBuffer, ip);
            mBuffer.Recycle();
            mBuffer = null;
        }

        private void Error(string message)
        {
            if (OnErrorEvent != null)
            {
                OnErrorEvent(message);
            }
        }

        public void RegisterHandler(int packet, OnCustomPacket onCustomPacket)
        {
            if (mMessageHandler != null)
            {
                mMessageHandler.Add(packet, onCustomPacket);
            }
        }

        public void UnregisterHandler(int packet)
        {
            if (mMessageHandler != null && mMessageHandler.ContainsKey(packet))
            {
                mMessageHandler.Remove(packet);
            }
        }
    }
}
