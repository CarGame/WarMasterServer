﻿using System.Collections.Generic;

public class CarCommonItemCache<T> where T : new()
{
    private LinkedList<T> mItemCacheList = new LinkedList<T>();
    private int mCacheCapacity = 50;

    public void RecycleItem(T t)
    {
        if (mItemCacheList.Count > mCacheCapacity)
        {
            mItemCacheList.RemoveFirst();
        }

        mItemCacheList.AddLast(t);
    }

    public T GetItem()
    {
        if (mItemCacheList.Count > 0)
        {
            T t = mItemCacheList.Last.Value;

            return t;
        }

        return default(T);
    }

    public void ClearItem()
    {
        mItemCacheList.Clear();
    }

    public void SetCacheCapacity(int capacity)
    {
        mCacheCapacity = capacity;
    }
}
